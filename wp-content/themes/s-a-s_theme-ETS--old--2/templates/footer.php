<footer id="footer" class="content-info navbar hidden-xs" role="contentinfo">
<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <?php dynamic_sidebar('sidebar-footer'); ?>
    </div>
  </div>
</div>
</footer>

<?php wp_footer(); ?>