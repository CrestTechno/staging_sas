<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>
    <header>
      <h1 class="entry-title"><?php the_title(); ?></h1>
      <?php // get_template_part('templates/entry-meta'); ?>
    </header>
    <div class="entry-content">
    
<div class="row">
  <div class="col-md-4"><?php if (get_post_meta( $post->ID, 'product_image', true ) != null) { 
         if(is_numeric(get_post_meta( $post->ID, 'product_image', true ))) { echo '<a href="' . get_permalink() . '">' .wp_get_attachment_image( get_post_meta( $post->ID, 'product_image', true ) ) . '</a>'; } else {?>
			<a href="<?php get_permalink(); ?>"><img class="img-responsive" src="<?php echo get_post_meta( $post->ID, 'product_image', true ); ?>" /></a>
		<?php } } 
		else { ?>
			echo 
				'<a href="<?php get_permalink(); ?>"><img class="img-responsive" src="/media/awaiting-photo.png" width="150" height="150" alt="Awaiting Image" /></a>';
		<?php }
    ?></div>
  <div class="col-md-4"><?php echo get_post_meta( $post->ID, 'product_shortdesc', true ); ?></div>
  <div class="col-md-4">
    <ul class="list-group">
    <li class="list-group-item">Product Code: <strong><?php echo get_post_meta( $post->ID, 'product_code', true ); ?></strong></li>
    <li class="list-group-item">Price: <?php if (get_post_meta( $post->ID, 'product_discount', true ) != '') { echo 'AU$ ' . get_post_meta( $post->ID, 'product_discount', true ); } else {echo 'AU$ ' . get_post_meta( $post->ID, 'product_price', true );} ?></li>
    <li class="list-group-item"><a type="button" href="#ordernow" class="btn btn-primary btn-lg btn-block">Order Now</a></li>
    <?php if (get_post_meta( $post->ID, 'product_sample', true ) != '') { ?><li class="list-group-item"><a type="button" href="<?php echo wp_get_attachment_url( get_post_meta( $post->ID, 'product_sample', true ) ); ?>" class="btn btn-default btn btn-block">Download Sample</a></li><? } ?>
    </ul>
  </div>
</div>  

<div class="row">
  <div class="col-md-12"><h3>Product Description</h3><p><?php echo get_post_meta( $post->ID, 'product_fulldesc', true ); ?></p></div>
  <?php global $wp_embed;?>
  <?php if (get_post_meta( $post->ID, 'product_video', true ) != '') { ?>
  <div class="col-md-12"><div class="videowrapper"><?php echo $wp_embed->run_shortcode('[embed]' . get_post_meta( $post->ID, 'product_video', true ) . '&wmode=opaque[/embed]'); ?></div></div>
  <?php } ?>
</div>

<div class="row">
  <div class="col-md-12"><h3>Product Gallery</h3></div>
  <div class="col-md-12"><?php echo do_shortcode('[justified_image_grid preset=5 row_height=100 height_deviation=5]'); ?></div>
</div>

<div id="ordernow">
  <h3>Make an Enquiry</h3>
  <?php echo FrmAppController::get_form_shortcode(array('id' => 27, 'key' => '', 'title' => false, 'description' => false, 'exclude_fields' => '')); ?>
  
</div>
    
      <?php the_content(); ?>
    </div>
    <footer>
      <?php wp_link_pages(array('before' => '<nav class="page-nav"><p>' . __('Pages:', 'ets'), 'after' => '</p></nav>')); ?>
    </footer>
    <?php comments_template('/templates/comments.php'); ?>
  </article>
<?php endwhile; ?>
