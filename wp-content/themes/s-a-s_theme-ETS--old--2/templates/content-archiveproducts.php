<article <?php post_class(); ?>>
  <header>
    <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
  </header>
<div class="row">
  <div class="col-md-4"><?php if (get_post_meta( $post->ID, 'product_image', true ) != null) { 
         if(is_numeric(get_post_meta( $post->ID, 'product_image', true ))) { echo '<a href="' . get_permalink() . '">' .wp_get_attachment_image( get_post_meta( $post->ID, 'product_image', true ) ) . '</a>'; } else {?>
			<a href="<?php get_permalink(); ?>"><img class="img-responsive" src="<?php echo get_post_meta( $post->ID, 'product_image', true ); ?>" /></a>
		<?php } } 
		else { ?>
			echo 
				'<a href="<?php get_permalink(); ?>"><img class="img-responsive" src="/media/awaiting-photo.png" width="150" height="150" alt="Awaiting Image" /></a>';
		<?php }
    ?></div>
  <div class="col-md-4"><?php echo get_post_meta( $post->ID, 'product_shortdesc', true ); ?></div>
  <div class="col-md-4">
    <ul class="list-group">
    <li class="list-group-item">Price: <?php if (get_post_meta( $post->ID, 'product_discount', true ) == '0') { echo 'AU$ ' . get_post_meta( $post->ID, 'product_price', true ); } ?></li>
    <li class="list-group-item"><a href="<?php the_permalink(); ?>"><button type="button" class="btn btn-primary btn-lg btn-block">View Product</button></a></li>
    </ul>
  </div>
</div>  
  
<div style="clear:both;"></div>
</article>