<header class="banner navbar navbar-default navbar-static-top" role="banner">
  <div class="container">
    <div class="title-header">
    	<a class="navbar-brand" href="<?php echo home_url(); ?>/"><img src="media/sas-logo.png" alt="<?php bloginfo('name'); ?>" /></a>
    </div>
  </div>

  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <nav class="collapse navbar-collapse" role="navigation">
      <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav', 'depth' => 3));
        endif;
      ?>
    <ul class="nav hidden-sm hidden-xs">
    <li><a href="http://www.youtube.com/user/sascomau/videos" class="pull-right"><img src="media/youtube.png" alt="S-A-S on YouTube" /></a></li>
    <li><a href="https://twitter.com/safetyadvisory" class="pull-right"><img src="media/twitter.png" alt="S-A-S on Twitter" /></a></li>
    <li><a href="http://www.linkedin.com/pub/safety-advisory-services/85/727/802" class="pull-right"><img src="media/linkedin.png" alt="S-A-S on LinkedIn" /></a></li>
    <li><a href="https://www.facebook.com/pages/Safety-Advisory-Services/472767356170256" class="pull-right"><img src="media/facebook.png" alt="S-A-S on YouTube" /></a></li>
    </ul>
    </nav>
  </div>

</header>
<?php if (get_post_meta( $post->ID, 'slider', true ) != '') {  ?>
<div class="container"><div class="slider-header"><?php putRevSlider(get_post_meta( $post->ID, 'slider', true )) ?></div></div>
<?php } ?>
<?php if (is_tax( 'product_type', 'e-ohs-documents' )) { ?>
<div class="container"><div class="slider-header"><?php putRevSlider('documents') ?></div></div>
<?php } ?>
<?php if (is_tax( 'product_type', 'e-ohs-products' )) { ?>
<div class="container"><div class="slider-header"><?php putRevSlider('products') ?></div></div>
<?php } ?>
<?php if (is_tax( 'product_type', 'e-ohs-software' )) { ?>
<div class="container"><div class="slider-header"><?php putRevSlider('software') ?></div></div>
<?php } ?>
