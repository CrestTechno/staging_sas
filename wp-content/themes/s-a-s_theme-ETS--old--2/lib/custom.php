<?php
/**
 * Custom functions
 */

function custom_scripts() {
  wp_enqueue_style('roots_custom', get_template_directory_uri() . '/assets/css/app.css');
}
add_action('wp_enqueue_scripts', 'custom_scripts', 200);

// Check permalink update,category base update and tag base update
add_filter('pre_update_option_' . 'category_base', 'wpms_remove_prefix_blog');
add_filter('pre_update_option_' . 'tag_base', 'wpms_remove_prefix_blog');
add_filter('pre_update_option_' . 'permalink_structure', 'wpms_remove_prefix_blog');

/**
* Just check if the current structure begins with /blog/ remove that and return the stripped structure
*/
function wpms_remove_prefix_blog($structure_permalink) {
        if ( substr($structure_permalink, 0, 6) != '/blog/' )
                return $structure_permalink;
        
        return substr($structure_permalink, 5, strlen($structure_permalink));
}


// Disables Kses only for textarea saves
foreach (array('pre_term_description', 'pre_link_description', 'pre_link_notes', 'pre_user_description') as $filter) {
	remove_filter($filter, 'wp_filter_kses');
}

// Disables Kses only for textarea admin displays
foreach (array('term_description', 'link_description', 'link_notes', 'user_description') as $filter) {
	remove_filter($filter, 'wp_kses_data');
}



// Pre-Populate Search Form Fields
add_filter('frm_setup_new_fields_vars', 'frm_populate_training_cat_tax', 20, 2);
add_filter('frm_setup_edit_fields_vars', 'frm_populate_training_cat_tax', 20, 2); //use this function on edit too
function frm_populate_training_cat_tax($values, $field){
if($field->id == 501){ //replace 125 with the ID of the field to populate
   $terms = get_terms( 'training_category', 'orderby=count&order=DESC&hide_empty=0' );
   unset($values['options']);
   $values['options'] = array(''); //remove this line if you are using a checkbox or radio button field
   foreach($terms as $term){
     $values['options'][$term->term_id] = $term->name . '( ' . $term->count . ' )';
   }
   $values['use_key'] = true; //this will set the field to save the post ID instead of post title
}
return $values;
}

add_filter('frm_setup_new_fields_vars', 'frm_populate_training_location_tax', 20, 2);
add_filter('frm_setup_edit_fields_vars', 'frm_populate_training_location_tax', 20, 2); //use this function on edit too
function frm_populate_training_location_tax($values, $field){
if($field->id == 502){ //replace 125 with the ID of the field to populate
   $terms = get_terms( 'training_locations', 'orderby=count&order=DESC&hide_empty=0' );
   unset($values['options']);
   $values['options'] = array(''); //remove this line if you are using a checkbox or radio button field
   foreach($terms as $term){
     $values['options'][$term->term_id] = $term->name . '( ' . $term->count . ' )' ;
   }
   $values['use_key'] = true; //this will set the field to save the post ID instead of post title
}
return $values;
}

/*
add_filter('frm_validate_field_entry', 'my_custom_validation', 8, 3);
function my_custom_validation($errors, $posted_field, $posted_value){
  if($posted_field->id == 495){ //change 25 to the ID of the hidden field
    $_POST['item_meta'][495] = $_POST['item_meta'][312] .' '. $_POST['item_meta'][313];
  }
  return $errors;
}
*/

// Combine first and last names for aweber
add_filter('frm_validate_field_entry', 'combine_two_fields', 8, 3);
function combine_two_fields($errors, $posted_field, $posted_value){
  if($posted_field->id == 495){ //change 25 to the ID of the hidden field (in two places)
    $_POST['item_meta'][495] = $_POST['item_meta'][312] .' '. $_POST['item_meta'][313];
    //change 20 and 21 to the IDs of your first and last name fields
  }
  return $errors;
}

// Add Facet for Listing by Company
function facetwp_index_company_row( $params ) {
if ( 'post_author' == $params['facet_source'] ) {
        $user_id = (int) $params['facet_value'];
        $company_name = get_user_meta($user_id, 'Company Name', true);
        $params['facet_display_value'] = $company_name;
    }
    return $params;
}
add_action( 'init', 'facet_init_function' );
function facet_init_function() {
    add_filter( 'facetwp_index_row', 'facetwp_index_company_row' );
}


/* New Additions Oct 2014 */

add_action('frm_after_create_entry', 'insert_id_in_product_field', 10, 2);
function insert_id_in_product_field($entry_id, $form_id){
//global $frmdb, $wpdb;
//global $frm_entry, $frm_field, $frm_entry_meta;
 if ( $form_id == 19 ){ // Add Entry ID to Product form entry
   $frm_entry_meta = new FrmEntryMeta();
   $frm_entry_meta->add_entry_meta($entry_id, 643, null, $entry_id); // change 25 to the id of the field
   //FrmProEntriesController::update_field_ajax($entry_id, 643, $entry_id); 
   //$wpdb->update($frmdb->entry_metas, array('meta_value' => $entry_id), array('item_id' => $entry_id, 'field_id' => 643));
 }
}
// Now we need to add the entry ID to the product post too for later use!
add_action('frm_after_create_entry', 'after_product_post_created', 41, 2);
function after_product_post_created($entry_id, $form_id){
  if($form_id == 19){ //change 5 to the ID of your form
    global $frm_entry;
    $entry = $frm_entry->getOne($entry_id);
    update_post_meta($entry->post_id, 'wpcf-product-entry-id', $entry_id); //change frm_entry_id to the name of your custom field
  }
}

add_action('frm_after_create_entry', 'insert_id_in_training_course_field', 10, 2); // DO the same with training courses
function insert_id_in_training_course_field($entry_id, $form_id){
//global $frmdb, $wpdb;
//global $frm_entry, $frm_field, $frm_entry_meta;
 if ( $form_id == 7 ){ // Add Entry ID to Training course form entry
   $frm_entry_meta = new FrmEntryMeta();
   $frm_entry_meta->add_entry_meta($entry_id, 691, null, $entry_id); // change 25 to the id of the field
   //FrmProEntriesController::update_field_ajax($entry_id, 643, $entry_id); 
   //$wpdb->update($frmdb->entry_metas, array('meta_value' => $entry_id), array('item_id' => $entry_id, 'field_id' => 643));
 }
}
// Now we need to add the entry ID to the product post too for later use!
add_action('frm_after_create_entry', 'after_training_course_post_created', 41, 2); // as above
function after_training_course_post_created($entry_id, $form_id){
  if($form_id == 7){ //change 5 to the ID of your form
    global $frm_entry;
    $entry = $frm_entry->getOne($entry_id);
    update_post_meta($entry->post_id, 'wpcf-training-course-entry-id', $entry_id); //change frm_entry_id to the name of your custom field
  }
}



add_action('frm_after_create_entry', 'insert_id_in_company_field', 10, 2);
function insert_id_in_company_field($entry_id, $form_id){
 if ( $form_id == 16 ){ // Add Entry ID to (product) Company form entry
   $frm_entry_meta = new FrmEntryMeta();
   $frm_entry_meta->add_entry_meta($entry_id, 650, null, $entry_id); // change 25 to the id of the field
   //FrmProEntriesController::update_field_ajax($entry_id, 650, $entry_id); 
   //$wpdb->update($frmdb->entry_metas, array('meta_value' => $entry_id), array('item_id' => $entry_id, 'field_id' => 650));
 }
 if ( $form_id == 8 ){ // Add Entry ID to (training) Company form entry
   $frm_entry_meta = new FrmEntryMeta();
   $frm_entry_meta->add_entry_meta($entry_id, 682, null, $entry_id); // change 25 to the id of the field
   //FrmProEntriesController::update_field_ajax($entry_id, 650, $entry_id); 
   //$wpdb->update($frmdb->entry_metas, array('meta_value' => $entry_id), array('item_id' => $entry_id, 'field_id' => 650));
 } 
}
add_action('frm_after_create_entry', 'insert_id_in_order_field', 10, 2);
function insert_id_in_order_field($entry_id, $form_id){
 if ( $form_id == 20 ){ // Add Entry ID to Order form entry
   $frm_entry_meta = new FrmEntryMeta();
   $frm_entry_meta->add_entry_meta($entry_id, 644, null, $entry_id); // change 25 to the id of the field
   //FrmProEntriesController::update_field_ajax($entry_id, 644, $entry_id); 
   //$wpdb->update($frmdb->entry_metas, array('meta_value' => $entry_id), array('item_id' => $entry_id, 'field_id' => 644));
 };
}
