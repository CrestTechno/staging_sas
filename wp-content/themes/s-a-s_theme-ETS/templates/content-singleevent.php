<?php while (have_posts()) : the_post(); ?>
<?php $location_terms = get_the_terms( get_the_ID(), 'training_locations' ); ?>
  <article <?php post_class(); ?>>
    <header>
      <h1 class="entry-title"><?php the_title(); ?></h1>
    <hr>
    </header>
    <div class="entry-content">
    <div class="col-md-4">
    <?php if (get_post_meta( $post->ID, 'wpcf-event-image', true ) != null) {
			echo 
				'<img src="' . get_post_meta( $post->ID, 'wpcf-event-image', true ) . '" class="img-responsive" alt="' . get_the_title(). '" />';
		}
		else {
			echo 
				'<img src="/media/awaiting-photo.png" class="img-responsive" alt="Awaiting Image" />';
		}
    ?>
    </div>
    <div class="col-md-4">
      <?php echo "Start Date: ".gmdate("F j, Y", get_post_meta($post->ID,'wpcf-event-start-date',TRUE))?><br>
      <?php echo "End Date: ".gmdate("F j, Y", get_post_meta($post->ID,'wpcf-event-end-date',TRUE))?><br>
      Locations: <?php foreach ( $location_terms as $term ) { echo $term->name .'<br> '; } ?>
    </div>
    <div class="col-md-4">
     <ul>
       <li><small>Organizer: <?php echo get_post_meta( $post->ID, 'wpcf-event-organiser', true ); ?></small></li>
       <li><small>Website: <a href="<?php echo get_post_meta( $post->ID, 'wpcf-event-website-link', true ); ?>">Website</a></small></li>
       <li><small>Facebook: <a href="<?php echo get_post_meta( $post->ID, 'wpcf-event-facebook-link', true ); ?>">Facebook</a></small></li>
       <li><small>Telephone: <?php echo get_post_meta( $post->ID, 'wpcf-event-phone-number', true ); ?></small></li>       
     </ul>
    </div>
    <div class="clearfix"></div>
    <hr>
    <h3>Event Details</h3>
      <div class="lead"><?php the_content(); ?></div>
    <hr>
    <?php global $wp_embed;?>
  <?php if (get_post_meta( $post->ID, 'wpcf-event-video-link', true ) != '') { ?>
  <div class="videowrapper"><?php echo $wp_embed->run_shortcode('[embed]' . get_post_meta( $post->ID, 'wpcf-event-video-link', true ) . '&wmode=opaque[/embed]'); ?></div>
  <?php } ?>  
    </div>
    <footer>
      <?php wp_link_pages(array('before' => '<nav class="page-nav"><p>' . __('Pages:', 'ets'), 'after' => '</p></nav>')); ?>
    </footer>
    <?php comments_template('/templates/comments.php'); ?>
  </article>
<?php endwhile; ?>
