<?php

/*

 Template Name:OHS Products



*/


get_header();



?>

<style>
#header,#footer{
	display:none;
}
.product-image img{
	width:100%;
	height:100%;
	}
span.onsale{
	display:none;
}	
.btn.btn-primary.btn-lg {
    margin-top: 20px;
}
ins {
    font-weight: bold;
}
</style>
<h1><?php the_title(); ?></h1>
<div class="row">
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title"><a class="accordion-toggle" href="#collapseOne" data-toggle="collapse" data-parent="#accordion">Products Information (Click to Close)</a></h4>
</div>
<div id="collapseOne" class=" in collapse">
<div class="panel-body">
<h2 class="col-md-6" style="text-align: center;"><span style="color: #993366;">THIS PRODUCT SITE IS STILL UNDER CONSTRUCTION [STANDBY]</span></h2>
<p>
At Safety Advisory Services we specialise in providing OHS Products related to safety, from the supply of work wear, footwear, personal protective equipment, hardware, tools and work place safety to traffic management road safety. You will find a complete range of OH&amp;S Products to select and purchase from on line. You will find shopping online with Safety Advisory Services a relaxing and time saving experience. From selecting your goods, going to the check out, paying online and tracking the delivery of your purchase to your shop front is meant to easy.
<!-- Button trigger modal -->
</p>
<button style="clear:both; margin:0px 0px 20px 18px;width:auto;float:left;" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#moreInfo">Read More...</button></div>
</div>
</div>
</div>
<!-- Modal -->
<div id="moreInfo" class="modal fade" tabindex="-1">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header"><button class="close" type="button" data-dismiss="modal">�</button>
<h4 id="moreInfoLabel" class="modal-title">Modal title</h4>
</div>
<div class="modal-body">

Get online to view the latest product information, check stock availability, review current pricing and submit orders � without even picking up the phone. It�s a simple, secure and a fast way to order.
<ul>
	<li><b>With our range of products:</b> finding the right one for you can be a daunting task, so we've upgraded to a world-leading search and information platform. The new platform makes information sharing easier and smarter.</li>
	<li><b>Online Store:</b> - Choose and Buy Online from the online store, check for prices, offers, specifications and availability.</li>
	<li><b>Our New Intelligent Search tools:</b> Start typing a keyword and the intelligent search function will seek out the best results and recommend relevant products using your key words.</li>
	<li><b>We�ve added more options:</b> to make searching quicker you can now search by keywords, product name, brands name and a range of other product attributes.</li>
	<li><b>Our new navigation function:</b> allows you to navigate through groups of products and also by attributes. Browse through our range by category, Item type � the combinations are limitless!</li>
	<li><b>Extensive Product Range:</b> - Huge range of safety products to meet your workplace safety requirements with no compromise with quality.</li>
	<li><b>Real Time Analysis:-</b> Focus on real issues like workers safety and comfort, better productivity and researches your work place to find the appropriate solutions for the safest environment.</li>
	<li><b>Professional Approach: -</b> Our team of experienced professionals to provide you effective advice and solutions to your individual need.</li>
	<li><b>Fast Guaranteed Service:</b> - Guaranteed same day shipping if you are ordering before 4 pm and delivering directly to your work place.</li>
	<li><b>Our achievements:-</b> Our customer�s satisfaction is our achievement at the end of the day by providing the best safety solutions achievable.</li>
</ul>
<b>Compare Products Side By Side</b>
<ul>
	<li>Can�t decide between two or more products: Use our comparison tool to measure up product features side by side. You can add your items to your cart straight after comparing.</li>
</ul>
</div>
<div class="modal-footer"><button class="btn btn-default" type="button" data-dismiss="modal">Close</button></div>
</div>
<!-- /.modal-content -->

</div>
<!-- /.modal-dialog -->

</div>
<!-- /.modal -->

<hr />




<div>
    <?php
        $args = array( 'post_type' => 'product', 'posts_per_page' => 10, 'product_cat' => 'ohs-products', 'orderby' => 'DESC' );
        $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>

          <article class="type-products status-publish hentry">
  <header>
    <h2 class="entry-title"><a href="<?php echo get_permalink( $loop->post->ID ) ?>" title="<?php echo esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID); ?>"><?php the_title(); ?></a></h2>
  </header>
  <div class="row">
    <div class="col-md-4 product-image"><?php woocommerce_show_product_sale_flash( $post, $product ); ?>

                        <?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail( $loop->post->ID,'full'); else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="Placeholder"  />'; ?></div>
    <div class="col-md-4"><?php the_content(); ?>.</div>
    <div class="col-md-4">
      <ul class="list-group">
        <li class="list-group-item">Price:<?php echo $product->get_price_html(); ?></li>
        <li class="list-group-item">
         <?php woocommerce_template_loop_add_to_cart( $loop->post, $product ); ?></li>
      </ul>
    </div>
  </div>
  <div style="clear:both;"></div>
</article>
<hr>


    <?php endwhile; ?>
    <?php wp_reset_query(); ?>
	
</div>

<div class="sharedaddy sd-sharing-enabled">
<div class="robots-nocontent sd-block sd-social sd-social-icon-text sd-sharing"><h3 class="sd-title">Share this:</h3><div class="sd-content"><ul><li class="share-linkedin"><a rel="nofollow" class="share-linkedin sd-button share-icon" href="http://nuancedi.wwwss10.a2hosted.com/s-a-s/ohs-products/?share=linkedin&amp;nb=1" target="_blank" title="Click to share on LinkedIn" id="sharing-linkedin-1687"><span>LinkedIn</span></a></li><li class="share-twitter"><a rel="nofollow" class="share-twitter sd-button share-icon" href="http://nuancedi.wwwss10.a2hosted.com/s-a-s/ohs-products/?share=twitter&amp;nb=1" target="_blank" title="Click to share on Twitter" id="sharing-twitter-1687"><span>Twitter</span></a></li><li class="share-facebook"><a rel="nofollow" class="share-facebook sd-button share-icon" href="http://nuancedi.wwwss10.a2hosted.com/s-a-s/ohs-products/?share=facebook&amp;nb=1" target="_blank" title="Share on Facebook" id="sharing-facebook-1687"><span>Facebook</span></a></li><li class="share-google-plus-1"><a rel="nofollow" class="share-google-plus-1 sd-button share-icon" href="http://nuancedi.wwwss10.a2hosted.com/s-a-s/ohs-products/?share=google-plus-1&amp;nb=1" target="_blank" title="Click to share on Google+" id="sharing-google-1687"><span>Google</span></a></li><li class="share-print"><a rel="nofollow" class="share-print sd-button share-icon" href="http://nuancedi.wwwss10.a2hosted.com/s-a-s/ohs-products/#print" target="_blank" title="Click to print"><span>Print</span></a></li><li class="share-end"></li></ul></div></div>
</div>






<?php

 get_footer();

?>