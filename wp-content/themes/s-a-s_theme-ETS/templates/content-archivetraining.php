<article <?php post_class(); ?>>
  <header>
    <h2 class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
  </header>
  
<div class="row">
  <div class="col-md-8">
  <h4>Course Overview</h4>
  <p><?php echo get_post_meta( $post->ID, 'course_overview', true ); ?></p>
  </div>
  <div class="col-md-4">
    <ul class="list-group">
    <li class="list-group-item">Location: <strong><?php echo get_post_meta( $post->ID, 'course_location', true ); ?></strong></li>
    <li class="list-group-item">Duration: <strong><?php echo get_post_meta( $post->ID, 'course_duration', true ); ?></strong></li>
    <li class="list-group-item"><a href="<?php the_permalink(); ?>"><button type="button" class="btn btn-primary btn-lg btn-block">View Course</button></a></li>
    </ul>
  </div>
</div>
  
<div style="clear:both;"></div>
</article>
