<?php if(!is_front_page() ) { ?>
<div class="page-header">
  <h1>
    <?php echo ets_title(); ?>
  </h1>
</div>
<p><?php echo category_description(); ?></p>
<?php } ?>