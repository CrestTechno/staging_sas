<?php $location_terms = get_the_terms( get_the_ID(), 'training_locations' ); ?>
<article <?php post_class(); ?>>
  <header>
    <div class="alert alert-info"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
  </header>
  
<div class="row">
  <div class="col-md-4">
<?php if (get_post_meta( $post->ID, 'wpcf-event-image', true ) != null) {
			echo 
				'<img src="' . get_post_meta( $post->ID, 'wpcf-event-image', true ) . '" class="img-responsive" alt="' . get_the_title(). '" />';
		}
		else {
			echo 
				'<img src="/media/awaiting-photo.png" class="img-responsive" alt="Awaiting Image" />';
		}
    ?>
  </div>
  <div class="col-md-4">
  <ul>
       <li><small>Organizer: <?php echo get_post_meta( $post->ID, 'wpcf-event-organiser', true ); ?></small></li>
       <li><small>Website: <a href="<?php echo get_post_meta( $post->ID, 'wpcf-event-website-link', true ); ?>">Website</a></small></li>
       <li><small>Facebook: <a href="<?php echo get_post_meta( $post->ID, 'wpcf-event-facebook-link', true ); ?>">Facebook</a></small></li>
       <li><small>Telephone: <?php echo get_post_meta( $post->ID, 'wpcf-event-phone-number', true ); ?></small></li>       
  </ul>
  </div>
  <div class="col-md-4">
  <?php echo "Start Date: ".gmdate("F j, Y", get_post_meta($post->ID,'wpcf-event-start-date',TRUE))?><br>
      <?php echo "End Date: ".gmdate("F j, Y", get_post_meta($post->ID,'wpcf-event-end-date',TRUE))?><br>
      Locations: <?php foreach ( $location_terms as $term ) { echo $term->name .'<br> '; } ?><br>
      <a class="btn btn-primary" href="<?php the_permalink(); ?>">View Event</a>
  </div>
</div>
  
<div style="clear:both;"></div>
</article>
<hr>