<?php

/*

 Template Name:OHS Documents



*/


get_header();



?>

<style>
#header,#footer{
	display:none;
}
.product-image img{
	width:100%;
	height:100%;
	}
span.onsale{
	display:none;
}	
.btn.btn-primary.btn-lg {
    margin-top: 20px;
}
ins {
    font-weight: bold;
}
</style>
<h1><?php the_title(); ?></h1>
<div class="row">
<div class="panel panel-default">
<div class="panel-heading">
<h4 class="panel-title"><a class="accordion-toggle" href="#collapseOne" data-toggle="collapse" data-parent="#accordion">eDocuments Information (Click to Close)</a></h4>
</div>
<div id="collapseOne" class=" in collapse">
<div class="panel-body">
<h2>E-Documents</h2>
<h2 class="col-md-6" style="text-align: center;"><span style="color: #993366;">THIS DOCUMENT SITE IS STILL UNDER CONSTRUCTION [STANDBY]</span></h2>
In this section of the site you can purchase OH&amp;S E-Documents online for immediate download. To purchase, select the E-Document just as you would a product and go through the online checkout. When you complete the online checkout process, you will receive a page that will provide you with the necessary links to download the E-Document you have purchased. Applicable sales tax is added, but shipping &amp; handling charges do not apply to E-Documents.
<!-- Button trigger modal -->
<button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#moreInfo">Read More...</button>

</div>
</div>
</div>
</div>
<!-- Modal -->
<div id="moreInfo" class="modal fade" tabindex="-1">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header"><button class="close" type="button" data-dismiss="modal">�</button>
<h4 id="moreInfoLabel" class="modal-title">OHS Documents</h4>
</div>
<div class="modal-body">

Our selection of E-Documents range�s from Safety Management Plans, Procedures, Guidance Notes,Work Method Statements and much much more.
<h4>Copyright Information</h4>
The electronic online versions of all S-A-S resources and information have the same copyright as the printed version; your purchase is for your use only, electronic or printed versions of S-A-S resources may not be shared with others without express written permission from Safety Advisory services P/L. You may use an E-Document on your computer and/or print it; however, it is illegal to email Safety Advisory Services E-Documents or to print and distribute multiple copies without permission.
<h4>Return/Refund Policy</h4>
There are no returns/refunds allowed on e-documents, unless defective. If you discover a quality issue with a downloaded document, have difficulty downloading a document, received an incorrect document, or the document was not as described on the page, please contact Safety Advisory services customer service to ask for a refund or replacement: To contact us go to the Contact us tab and follow the prompts. A consultant will contact you within 24 hours of your request.
<h4>E-Document Formats</h4>
S-A-S E-Documents may be in Excel, Word, and PowerPoint and any other Office format.

</div>
<div class="modal-footer"><button class="btn btn-default" type="button" data-dismiss="modal">Close</button></div>
</div>
<!-- /.modal-content -->

</div>
<!-- /.modal-dialog -->

</div>
<!-- /.modal -->

<hr />


<div>
    <?php
        $args = array( 'post_type' => 'product', 'posts_per_page' => 10, 'product_cat' => 'ohs-documents', 'orderby' => 'DESC' );
        $loop = new WP_Query( $args );
        while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>

          <article class="type-products status-publish hentry">
  <header>
    <h2 class="entry-title"><a href="<?php echo get_permalink( $loop->post->ID ) ?>" title="<?php echo esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID); ?>"><?php the_title(); ?></a></h2>
  </header>
  <div class="row">
    <div class="col-md-4 product-image"><?php woocommerce_show_product_sale_flash( $post, $product ); ?>

                        <?php if (has_post_thumbnail( $loop->post->ID )) echo get_the_post_thumbnail( $loop->post->ID,'full'); else echo '<img src="'.woocommerce_placeholder_img_src().'" alt="Placeholder"  />'; ?></div>
    <div class="col-md-4"><?php the_content(); ?>.</div>
    <div class="col-md-4">
      <ul class="list-group">
        <li class="list-group-item">Price:<?php echo $product->get_price_html(); ?></li>
        <li class="list-group-item">
         <?php woocommerce_template_loop_add_to_cart( $loop->post, $product ); ?></li>
      </ul>
    </div>
  </div>
  <div style="clear:both;"></div>
</article>
<hr>


    <?php endwhile; ?>
    <?php wp_reset_query(); ?>
	
</div>

<div class="sharedaddy sd-sharing-enabled">
<div class="robots-nocontent sd-block sd-social sd-social-icon-text sd-sharing"><h3 class="sd-title">Share this:</h3><div class="sd-content"><ul><li class="share-linkedin"><a rel="nofollow" class="share-linkedin sd-button share-icon" href="http://nuancedi.wwwss10.a2hosted.com/s-a-s/ohs-products/?share=linkedin&amp;nb=1" target="_blank" title="Click to share on LinkedIn" id="sharing-linkedin-1687"><span>LinkedIn</span></a></li><li class="share-twitter"><a rel="nofollow" class="share-twitter sd-button share-icon" href="http://nuancedi.wwwss10.a2hosted.com/s-a-s/ohs-products/?share=twitter&amp;nb=1" target="_blank" title="Click to share on Twitter" id="sharing-twitter-1687"><span>Twitter</span></a></li><li class="share-facebook"><a rel="nofollow" class="share-facebook sd-button share-icon" href="http://nuancedi.wwwss10.a2hosted.com/s-a-s/ohs-products/?share=facebook&amp;nb=1" target="_blank" title="Share on Facebook" id="sharing-facebook-1687"><span>Facebook</span></a></li><li class="share-google-plus-1"><a rel="nofollow" class="share-google-plus-1 sd-button share-icon" href="http://nuancedi.wwwss10.a2hosted.com/s-a-s/ohs-products/?share=google-plus-1&amp;nb=1" target="_blank" title="Click to share on Google+" id="sharing-google-1687"><span>Google</span></a></li><li class="share-print"><a rel="nofollow" class="share-print sd-button share-icon" href="http://nuancedi.wwwss10.a2hosted.com/s-a-s/ohs-products/#print" target="_blank" title="Click to print"><span>Print</span></a></li><li class="share-end"></li></ul></div></div>
</div>






<?php

 get_footer();

?>