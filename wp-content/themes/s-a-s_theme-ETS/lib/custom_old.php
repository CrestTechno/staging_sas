<?php
/**
 * Custom functions
 */

function custom_scripts() {
  wp_enqueue_style('roots_custom', get_template_directory_uri() . '/assets/css/app.css');
}
add_action('wp_enqueue_scripts', 'custom_scripts', 200);

// Check permalink update,category base update and tag base update
add_filter('pre_update_option_' . 'category_base', 'wpms_remove_prefix_blog');
add_filter('pre_update_option_' . 'tag_base', 'wpms_remove_prefix_blog');
add_filter('pre_update_option_' . 'permalink_structure', 'wpms_remove_prefix_blog');

/**
* Just check if the current structure begins with /blog/ remove that and return the stripped structure
*/
function wpms_remove_prefix_blog($structure_permalink) {
        if ( substr($structure_permalink, 0, 6) != '/blog/' )
                return $structure_permalink;
        
        return substr($structure_permalink, 5, strlen($structure_permalink));
}


// Disables Kses only for textarea saves
foreach (array('pre_term_description', 'pre_link_description', 'pre_link_notes', 'pre_user_description') as $filter) {
	remove_filter($filter, 'wp_filter_kses');
}

// Disables Kses only for textarea admin displays
foreach (array('term_description', 'link_description', 'link_notes', 'user_description') as $filter) {
	remove_filter($filter, 'wp_kses_data');
}



// Pre-Populate Search Form Fields
add_filter('frm_setup_new_fields_vars', 'frm_populate_training_cat_tax', 20, 2);
add_filter('frm_setup_edit_fields_vars', 'frm_populate_training_cat_tax', 20, 2); //use this function on edit too
function frm_populate_training_cat_tax($values, $field){
if($field->id == 501){ //replace 125 with the ID of the field to populate
   $terms = get_terms( 'training_category', 'orderby=count&order=DESC&hide_empty=0' );
   unset($values['options']);
   $values['options'] = array(''); //remove this line if you are using a checkbox or radio button field
   foreach($terms as $term){
     $values['options'][$term->term_id] = $term->name . '( ' . $term->count . ' )';
   }
   $values['use_key'] = true; //this will set the field to save the post ID instead of post title
}
return $values;
}

add_filter('frm_setup_new_fields_vars', 'frm_populate_training_location_tax', 20, 2);
add_filter('frm_setup_edit_fields_vars', 'frm_populate_training_location_tax', 20, 2); //use this function on edit too
function frm_populate_training_location_tax($values, $field){
if($field->id == 502){ //replace 125 with the ID of the field to populate
   $terms = get_terms( 'training_locations', 'orderby=count&order=DESC&hide_empty=0' );
   unset($values['options']);
   $values['options'] = array(''); //remove this line if you are using a checkbox or radio button field
   foreach($terms as $term){
     $values['options'][$term->term_id] = $term->name . '( ' . $term->count . ' )' ;
   }
   $values['use_key'] = true; //this will set the field to save the post ID instead of post title
}
return $values;
}

/*
add_filter('frm_validate_field_entry', 'my_custom_validation', 8, 3);
function my_custom_validation($errors, $posted_field, $posted_value){
  if($posted_field->id == 495){ //change 25 to the ID of the hidden field
    $_POST['item_meta'][495] = $_POST['item_meta'][312] .' '. $_POST['item_meta'][313];
  }
  return $errors;
}
*/
add_filter('frm_validate_field_entry', 'combine_two_fields', 8, 3);
function combine_two_fields($errors, $posted_field, $posted_value){
  if($posted_field->id == 495){ //change 25 to the ID of the hidden field (in two places)
    $_POST['item_meta'][495] = $_POST['item_meta'][312] .' '. $_POST['item_meta'][313];
    //change 20 and 21 to the IDs of your first and last name fields
  }
  return $errors;
}