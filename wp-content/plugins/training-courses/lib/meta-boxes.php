<?php
/**
 * Custom meta boxes with the CMB plugin
 *
 * @link https://github.com/jaredatch/Custom-Metaboxes-and-Fields-for-WordPress
 * @link https://github.com/jaredatch/Custom-Metaboxes-and-Fields-for-WordPress/wiki/Basic-Usage
 * @link https://github.com/jaredatch/Custom-Metaboxes-and-Fields-for-WordPress/wiki/Field-Types
 * @link https://github.com/jaredatch/Custom-Metaboxes-and-Fields-for-WordPress/wiki/Display-Options
 */

function base_meta_boxes($meta_boxes) {
  /**
   * Page Options meta box
   */
  $meta_boxes[] = array(
    'id'         => 'page_options',
    'title'      => 'Training Course Information',
    'pages'      => array( 'training_courses', ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'fields'     => array(
      array(
				'name' => 'Training Course Information',
				'desc' => 'Information about the Training Course you are adding',
				'id'   => $prefix . 'course_title',
				'type' => 'title',
			),
	   array(
				'name' => 'Course Code',
				'desc' => 'Unique Course ID Code',
				'id'   => $prefix . 'course_code',
				'type' => 'text',
			),	
	   array(
				'name' => 'Course Duration',
				'desc' => 'Duration of the Training Course',
				'id'   => $prefix . 'course_duration',
				'type' => 'text',
			),	
	   array(
				'name' => 'Course Price',
				'desc' => 'Price for the course (A$)',
				'id'   => $prefix . 'course_price',
				'type' => 'text',
			),	
      array(
				'name' => 'Training Detail Section',
				'desc' => 'Details about the training course',
				'id'   => $prefix . 'course_details',
				'type' => 'title',
			),
	   array(
				'name' => 'Course Venue Location',
				'desc' => 'Location of the Training Course',
				'id'   => $prefix . 'course_location',
				'type' => 'text',
			),
	   array(
				'name' => 'Course Start Time',
				'desc' => 'Starting Time of the Course',
				'id'   => $prefix . 'course_starttime',
				'type' => 'text_time',
			),
	   array(
				'name' => 'Course Start Date',
				'desc' => 'Starting Date of the Course',
				'id'   => $prefix . 'course_startdate',
				'type' => 'text_date',
			),
	   array(
				'name' => 'Course End Time',
				'desc' => 'Ending Time of the Course',
				'id'   => $prefix . 'course_endtime',
				'type' => 'text_time',
			),
	   array(
				'name' => 'Course End Date',
				'desc' => 'Ending Date of the Course',
				'id'   => $prefix . 'course_enddate',
				'type' => 'text_date',
			),
	  array(
				'name'    => 'Training Course Delivery Method',
				'desc'    => 'How the course will be delivered',
				'id'      => $prefix . 'course_delivery',
				'type'    => 'multicheck',
				'options' => array(
					'Instructor Led' => 'Instructor Led',
					'Instructor Assisted (mentored)' => 'Instructor Assisted', 
					'Virtual Instructor Led' => 'Virtual Instructor Led', 
					'Webinar' => 'Webinar', 
					'Private One-on-One' => 'Private One-on-One', 
					'Virtual Private One-on-One' => 'Virtual Private One-on-One' 
				),
			),
	   array(
				'name' => 'Course Overview',
				'desc' => 'overview of the Training Course',
				'id'   => $prefix . 'course_overview',
				'type' => 'textarea_small',
			),
	   array(
				'name' => 'Course Pre-Requirements',
				'desc' => 'Requirements of the Training Course',
				'id'   => $prefix . 'course_requirements',
				'type' => 'text',
			),
	   array(
				'name' => 'Course Accreditations',
				'desc' => 'Accreditations of the Training Course',
				'id'   => $prefix . 'course_accreditations',
				'type' => 'text',
			),
		array(
				'name'    => 'Course Information',
				'desc'    => 'Full Information on the Training Course',
				'id'      => $prefix . 'course_information',
				'type'    => 'wysiwyg',
				'options' => array(	'textarea_rows' => 5, ),
			),
      array(
				'name' => 'Course Promotion',
				'desc' => 'Special Offers or Pricing for this Training Course',
				'id'   => $prefix . 'course_promotion',
				'type' => 'title',
			),
	   array(
				'name' => 'Promotion Price',
				'desc' => 'Special Promo Price (A$)',
				'id'   => $prefix . 'course_promoprice',
				'type' => 'text',
			),
	   array(
				'name' => 'Promotion Information',
				'desc' => 'Information about the Promotion',
				'id'   => $prefix . 'course_promoinfo',
				'type' => 'textarea_small',
			),
    ),
  );

  return $meta_boxes;
}
add_filter('cmb_meta_boxes', 'base_meta_boxes');
