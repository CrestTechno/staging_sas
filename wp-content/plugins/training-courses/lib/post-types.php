<?php
/**
 * Custom post types & taxonomies
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 * @link http://codex.wordpress.org/Function_Reference/register_taxonomy
 */

/**
 * Training Course custom post type
 */
function register_training_course_post_type() {
  $labels = array(
    'name'               => 'Training Courses',
    'singular_name'      => 'Training Course',
    'add_new'            => 'Add New',
    'add_new_item'       => 'Add New Training Course',
    'edit_item'          => 'Edit Training Course',
    'new_item'           => 'New Training Course',
    'view_item'          => 'View Training Course',
    'search_items'       => 'Search Training Courses',
    'not_found'          => 'No Training Courses found',
    'not_found_in_trash' => 'No Training Courses found in trash',
    'parent_item_colon'  => '',
    'menu_name'          => 'Training Courses'
  );

  $args = array(
    'labels'              => $labels,
    'public'              => true,
    'exclude_from_search' => false,
    'publicly_queryable'  => true,
    'show_ui'             => true,
    'show_in_menu'        => true,
    'query_var'           => true,
    'rewrite'             => array('slug' => 'training_courses'),
    'capability_type'     => 'post',
    'has_archive'         => true,
    'hierarchical'        => true,
    'menu_position'       => null,
    'supports'            => array('title', 'page-attributes')
  );

  register_post_type('training_courses', $args);
}
add_action('init', 'register_training_course_post_type');

/**
 * Training Course State/Region taxonomy
 */
function register_training_region_taxonomy() {
  $labels = array(
    'name'              => 'State/Region',
    'singular_name'     => 'State/Region',
    'search_items'      => 'Search States/Regions',
    'all_items'         => 'All States/Regions',
    'parent_item'       => 'Parent State/Region',
    'parent_item_colon' => 'Parent State/Region:',
    'edit_item'         => 'Edit State/Region',
    'update_item'       => 'Update State/Region',
    'add_new_item'      => 'Add New State/Region',
    'new_item_name'     => 'New State/Region Name',
    'menu_name'         => 'State/Region'
  );

  $args = array(
    'hierarchical' => true,
    'labels'       => $labels,
    'show_ui'      => true,
    'query_var'    => true,
    'rewrite'      => array('slug' => 'location'),
  );
  register_taxonomy('training_locations', 'training_courses', $args);
}
add_action('init', 'register_training_region_taxonomy');

/**
 * Training Course Category taxonomy
 */
function register_training_category_taxonomy() {
  $labels = array(
    'name'              => 'Category',
    'singular_name'     => 'Category',
    'search_items'      => 'Search Categories',
    'all_items'         => 'All Categories',
    'parent_item'       => 'Parent Category',
    'parent_item_colon' => 'Parent Category:',
    'edit_item'         => 'Edit Category',
    'update_item'       => 'Update Category',
    'add_new_item'      => 'Add New Category',
    'new_item_name'     => 'New Category Name',
    'menu_name'         => 'Category'
  );

  $args = array(
    'hierarchical' => true,
    'labels'       => $labels,
    'show_ui'      => true,
    'query_var'    => true,
    'rewrite'      => array('slug' => 'training-category'),
  );
  register_taxonomy('training_category', 'training_courses', $args);
}
add_action('init', 'register_training_category_taxonomy');
