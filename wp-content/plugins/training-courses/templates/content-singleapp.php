<?php
/**
 * The template for displaying content in the single.php template
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 * @since Twenty Eleven 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<h1 class="entry-title"><?php the_title(); ?> <?php echo get_post_meta( $post->ID, 'app_version', true ); ?></h1>
<div id="slides">
<?php
$thumb_ID = get_post_thumbnail_id( $post->ID );
$argsThumb = array(
    'order'          => 'ASC',
    'post_type'      => 'attachment',
    'post_parent'    => $post->ID,
    'post_mime_type' => 'image',
	'exclude' => $thumb_ID,
    'post_status'    => null
);
$attachments = get_posts($argsThumb);
if ($attachments) {
    foreach ($attachments as $attachment) {
        //echo apply_filters('the_title', $attachment->post_title);
        echo '<img src="'.wp_get_attachment_url($attachment->ID).'" />';
    }
}
?>
</div>


		<?php if ( 'post' == get_post_type() ) : ?>
		<div class="entry-meta">
			<?php twentyeleven_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<h3>App Description</h3>
        <div class="sideinfobox"><img class="sideappthumb" src="<?php echo get_post_meta( $post->ID, 'app_icon', true ); ?>" /><p style="font-weight:bold;">About the App</p>        
        <p>Platform: <?php echo get_the_term_list( $post->ID, 'base_app_platform', '', ', ' ) ?></p>
        <p>Category: <?php echo get_the_term_list( $post->ID, 'base_app_category', '', ', ' ) ?> </p>
        <p>Version: <?php echo get_post_meta( $post->ID, 'app_version', true ); ?> </p>
        <p>Added: <?php the_date(); ?></p>
        <p>Compatible with: <?php $total = count(get_post_meta( $post->ID, 'app_compatible', false )); foreach (get_post_meta( $post->ID, 'app_compatible', false ) as $compatible) {$i++;echo $compatible;if ($i != $total) echo', '; ;} ?></p>
        <p>Developer: <?php echo get_the_term_list( $post->ID, 'base_app_developer', '', ', ' ) ?></p>
        <?php if (get_post_meta( $post->ID, 'app_iosdl', true ) != '') { ?><p><a href="http://<?php echo get_post_meta( $post->ID, 'app_iosdl', true ); ?>"><button>DOWNLOAD iOS</button></a></p><?php } ?>
        <?php if (get_post_meta( $post->ID, 'app_androiddl', true ) != '') { ?><p><a href="http://<?php echo get_post_meta( $post->ID, 'app_androiddl', true ); ?>"><button>DOWNLOAD Android</button></a></p><?php } ?>
        <?php if (get_post_meta( $post->ID, 'app_winphonedl', true ) != '') { ?><p><a href="http://<?php echo get_post_meta( $post->ID, 'app_winphonedl', true ); ?>"><button>DOWNLOAD WinPhone</button></a></p><?php } ?>
        <h4>Version Archive: </h4>
<ul style="list-style-image:none;">
<?php
  $ancestors = get_post_ancestors($post->ID);
  $parent = $ancestors[0];

  if($parent) { //if its a CHILD page
    echo wp_list_pages("title_li=&include=".$parent."&echo=0&post_type=base_apps");
    $children = get_pages("title_li=&child_of=".$parent."&echo=0&post_type=base_apps");

}  else { //if it's a PARENT page
    echo wp_list_pages("title_li=&include=".get_the_ID()."&echo=0&post_type=base_apps");
    $children = get_pages("title_li=&child_of=".get_the_ID()."&echo=0&post_type=base_apps");
}
  if ($children) { 
  //print_r($children);
  foreach ($children as $child) {
	echo '<li><a href="/apps/'.$child->post_name.'">'.$child->post_title.'</a></li>';
  } ?>
  </ul>
  <?php } ?>
        
    </div>
		<?php the_content(); ?>
		<?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . __( 'Pages:', 'twentyeleven' ) . '</span>', 'after' => '</div>' ) ); ?>
	</div><!-- .entry-content -->

	<footer class="entry-meta">
		<?php
			/* translators: used between list items, there is a space after the comma */
			$categories_list = get_the_category_list( __( ', ', 'twentyeleven' ) );

			/* translators: used between list items, there is a space after the comma */
			$tag_list = get_the_tag_list( '', __( ', ', 'twentyeleven' ) );
			if ( '' != $tag_list ) {
				$utility_text = __( 'This entry was posted in %1$s and tagged %2$s by <a href="%6$s">%5$s</a>. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'twentyeleven' );
			} elseif ( '' != $categories_list ) {
				$utility_text = __( 'This entry was posted in %1$s by <a href="%6$s">%5$s</a>. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'twentyeleven' );
			} else {
				$utility_text = __( 'This entry was posted by <a href="%6$s">%5$s</a>. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>.', 'twentyeleven' );
			}

			printf(
				$utility_text,
				$categories_list,
				$tag_list,
				esc_url( get_permalink() ),
				the_title_attribute( 'echo=0' ),
				get_the_author(),
				esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) )
			);
		?>
		<?php edit_post_link( __( 'Edit', 'twentyeleven' ), '<span class="edit-link">', '</span>' ); ?>

		<?php if ( get_the_author_meta( 'description' ) && ( ! function_exists( 'is_multi_author' ) || is_multi_author() ) ) : // If a user has filled out their description and this is a multi-author blog, show a bio on their entries ?>
		<div id="author-info">
			<div id="author-avatar">
				<?php echo get_avatar( get_the_author_meta( 'user_email' ), apply_filters( 'twentyeleven_author_bio_avatar_size', 68 ) ); ?>
			</div><!-- #author-avatar -->
			<div id="author-description">
				<h2><?php printf( __( 'About %s', 'twentyeleven' ), get_the_author() ); ?></h2>
				<?php the_author_meta( 'description' ); ?>
				<div id="author-link">
					<a href="<?php echo esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ); ?>" rel="author">
						<?php printf( __( 'View all posts by %s <span class="meta-nav">&rarr;</span>', 'twentyeleven' ), get_the_author() ); ?>
					</a>
				</div><!-- #author-link	-->
			</div><!-- #author-description -->
		</div><!-- #author-info -->
		<?php endif; ?>
	</footer><!-- .entry-meta -->
</article><!-- #post-<?php the_ID(); ?> -->
<script>
    jQuery(function(){
      jQuery("#slides").slidesjs({
	  pagination: {
      active: false,
      effect: "slide"
    },
	play: {
      active: false,
      effect: "slide",
      interval: 2500,
      auto: true,
      swap: true,
      pauseOnHover: false
    },
		navigation: {
      active: false,
      effect: "slide"
    },
        width: 940,
        height: 528
      });
    });
</script>
