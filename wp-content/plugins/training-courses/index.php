<?php
/*
Plugin Name: Training Course System Plugin
Plugin URI: http://www.s-a-s.com.au
Description: Custom Training Course Listing System for www.s-a-s.com.au
Version: 1.0
Author: ETS.io
Author URI: http://ets.io
*/

//require WP_PLUGIN_DIR . '/options-framework-plugin/options-framework.php';
require WP_PLUGIN_DIR . '/training-courses/wp-post-formats/cf-post-formats.php';

function plugin_scripts()  
    {  
        wp_register_script( 'app-script', plugins_url( '/js/appjs.js', __FILE__ ) );  
		wp_register_script( 'app-qtip', plugins_url( '/js/jquery.qtip.min.js', __FILE__ ) );  
		wp_register_script( 'app-slides', plugins_url( '/js/jquery.slides.min.js', __FILE__ ) ); 
		//wp_register_script( 'app-imagesloaded', plugins_url( '/js/imagesloaded.min.js', __FILE__ ) );
 		wp_register_style( 'app-style', plugins_url( '/css/appstyle.css', __FILE__ ), array(), '20120208', 'all' ); 
		wp_register_style( 'app-qtipstyle', plugins_url( '/css/jquery.qtip.min.css', __FILE__ ), array(), '20120208', 'all' ); 

        wp_enqueue_script( 'app-script' ); 
		wp_enqueue_script( 'app-qtip' );
		wp_enqueue_script( 'app-slides' );
		//wp_enqueue_script( 'app-imagesloaded' );
		wp_enqueue_style( 'app-style' ); 
		wp_enqueue_style( 'app-qtipstyle' ); 
    }  
add_action( 'wp_enqueue_scripts', 'plugin_scripts' );  


// Load CMB
function load_cmb() {
  if (!is_admin()) {
    return;
  }

  require WP_PLUGIN_DIR . '/training-courses/cmb/init.php';
}
add_action('init', 'load_cmb');

require_once(dirname(__FILE__) . '/lib/post-types.php');
require_once(dirname(__FILE__) . '/lib/meta-boxes.php');
// require_once(dirname(__FILE__) . '/lib/shortcodes.php');

function get_images_from_media_library() {
global $post;
$query_images = get_children( array( 'post_parent' => get_the_ID(), 'post_type' => 'attachment', 'post_mime_type' => 'image', 'orderby' => 'menu_order', 'order' => 'ASC', 'numberposts' => 999 ) ); 

    $query_images = get_children( $args );
    $images = array();
    foreach ( $query_images as $image) {
        $images[]= $image->guid;
    }
    return $images;
}

function display_images_from_media_library() {

	$imgs = get_images_from_media_library();
	$html = '<div id="slides">';
	
	foreach($imgs as $img) {
	
		$html .= '<img src="' . $img . '" alt="" />';
	
	}
	
	$html .= '</div>';
	
	return $html;

}

function show_all_children( $post_id, $current_level ) {
    $children = get_posts( array(
        'post_type' =>'base_apps',
        'posts_per_page' =>-1,
        'post_parent' => $post_id,
        'order_by' => 'title',
        'order' => 'ASC' ) );
    if ( empty($children) ) return;

    echo '<ul class="children level-'.$current_level.'-children">';

    foreach ($children as $child) {

        /* Here would be the point where you
            do whatever you want to display the 
            posts. The variable $current_level can
            be used if you want to style different 
            levels in the hierarchy differently */

            echo '<li>';

        echo '<a href="'.get_permalink($child->ID). $child->ID . '">';
        echo apply_filters( 'the_title', $child->post_title );
        echo '</a>';

        // now call the same function for child of this child
        //show_all_children( $child->ID, $current_level+1 );

            echo '</li>';

        }

    echo '</ul>';
wp_reset_query();
    }
	
function show_all_parents( $post_id, $current_level ) {
    $children = get_posts( array(
        'post_type' =>'base_apps',
        'posts_per_page' =>1,
        'post_child' => $post_id,
        'order_by' => 'title',
        'order' => 'ASC' ) );
    if ( empty($children) ) return;

    echo '<ul class="children level-'.$current_level.'-children">';

                    echo '<li>';

        echo '<a href="'.get_permalink($children[0]->ID).'">';
        echo apply_filters( 'the_title', $children[0]->post_title );
        echo '</a>';

        // now call the same function for child of this child
        //show_all_parents( $children[0]->ID, $current_level+1 );

            echo '</li>';


    echo '</ul>';
wp_reset_query();
    }