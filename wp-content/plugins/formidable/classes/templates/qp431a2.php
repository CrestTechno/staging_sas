<?php     
$values['name'] = 'Add Training Course';
$values['description'] = '';
$values['editable'] = 0;
$values['logged_in'] = 0;
$values['options'] = array();
$values['options']['email_to'] = ''; 
$values['options']['submit_value'] = 'Submit'; 
$values['options']['success_msg'] = 'Your responses were successfully submitted. Thank you!';
$values['options']['show_form'] = 0;
$values['options']['akismet'] = '';
$values['options']['custom_style'] = 1;
$values['options']['before_html'] = '[if form_name]<h3>[form_name]</h3>[/if form_name]
[if form_description]<div class=\"frm_description\">[form_description]</div>[/if form_description]';
$values['options']['after_html'] = '';
$values['options']['submit_html'] = '<div class=\"frm_submit\">
[if back_button]<input type=\"submit\" value=\"[back_label]\" name=\"frm_prev_page\" formnovalidate=\"formnovalidate\" [back_hook] />[/if back_button]
<input type=\"submit\" value=\"[button_label]\" [button_action] />
<img class=\"frm_ajax_loading\" src=\"http://demosite.com/wp-content/plugins/formidable/images/ajax_loader.gif\" alt=\"Sending\"/>
</div>';
$values['options']['single_entry'] = 0;
$values['options']['single_entry_type'] = 'user';
$values['options']['logged_in_role'] = '';
$values['options']['editable_role'] = '';
$values['options']['open_editable'] = 0;
$values['options']['open_editable_role'] = '';
$values['options']['edit_value'] = 'Update';
$values['options']['edit_msg'] = 'Your submission was successfully saved.';

$values['options']['plain_text'] = 0;
//$values['options']['reply_to'] = '';
//$values['options']['reply_to_name'] = '';
$values['options']['email_subject'] = '';
$values['options']['email_message'] = '[default-message]';
$values['options']['inc_user_info'] = 0;

$values['options']['auto_responder'] = 0;
$values['options']['ar_plain_text'] = 0;
//$values['options']['ar_email_to'] = '';
$values['options']['ar_reply_to'] = '';
$values['options']['ar_reply_to_name'] = '';
$values['options']['ar_email_subject'] = '';
$values['options']['ar_email_message'] = '';


if ($form){
    $form_id = $form->id;
    $frm_form->update($form_id, $values );
    $form_fields = $frm_field->getAll(array('fi.form_id' => $form_id), 'field_order');
    if (!empty($form_fields)){
        foreach ($form_fields as $field)
            $frm_field->destroy($field->id);
    }
}else
    $form_id = $frm_form->create( $values );

    
$field_values = apply_filters('frm_before_field_created', FrmFieldsHelper::setup_new_vars('text', $form_id));
$field_values['id'] = 85;
$field_values['field_key'] = 'tc_title2';
$field_values['name'] = 'Course Name / Title';
$field_values['description'] = '';
$field_values['type'] = 'text';
$field_values['default_value'] = 'Course Name / Title';
$field_values['options'] = '';
$field_values['required'] = '1';
$field_values['field_order'] = '1';
$field_values['field_options']['size'] = '';
$field_values['field_options']['max'] = '';
$field_values['field_options']['label'] = '';
$field_values['field_options']['blank'] = 'This field cannot be blank.';
$field_values['field_options']['required_indicator'] = '*';
$field_values['field_options']['invalid'] = '';
$field_values['field_options']['separate_value'] = '0';
$field_values['field_options']['clear_on_focus'] = '1';
$field_values['field_options']['default_blank'] = '1';
$field_values['field_options']['classes'] = '';
$field_values['field_options']['custom_html'] = '<div id=\"frm_field_[id]_container\" class=\"frm_form_field form-field [required_class][error_class]\">
    <label class=\"frm_primary_label\">[field_name]
        <span class=\"frm_required\">[required_label]</span>
    </label>
    [input]
    [if description]<div class=\"frm_description\">[description]</div>[/if description]
    [if error]<div class=\"frm_error\">[error]</div>[/if error]
</div>';
$field_values['field_options']['slide'] = '0';
$field_values['field_options']['form_select'] = '';
$field_values['field_options']['show_hide'] = 'show';
$field_values['field_options']['any_all'] = 'any';
$field_values['field_options']['align'] = 'block';
$field_values['field_options']['hide_field'] = 'a:0:{}';
$field_values['field_options']['hide_field_cond'] = 'a:1:{i:0;s:2:"==";}';
$field_values['field_options']['hide_opt'] = 'a:0:{}';
$field_values['field_options']['star'] = '0';
$field_values['field_options']['ftypes'] = 'a:0:{}';
$field_values['field_options']['data_type'] = '';
$field_values['field_options']['restrict'] = '0';
$field_values['field_options']['start_year'] = '2000';
$field_values['field_options']['end_year'] = '2020';
$field_values['field_options']['read_only'] = '0';
$field_values['field_options']['admin_only'] = '';
$field_values['field_options']['locale'] = '';
$field_values['field_options']['attach'] = '';
$field_values['field_options']['minnum'] = '0';
$field_values['field_options']['maxnum'] = '9999';
$field_values['field_options']['step'] = '1';
$field_values['field_options']['clock'] = '12';
$field_values['field_options']['start_time'] = '00:00';
$field_values['field_options']['end_time'] = '23:59';
$field_values['field_options']['unique'] = '0';
$field_values['field_options']['use_calc'] = '0';
$field_values['field_options']['calc'] = '';
$field_values['field_options']['duplication'] = '1';
$field_values['field_options']['rte'] = 'mce';
$field_values['field_options']['dyn_default_value'] = '';
$field_values['field_options']['multiple'] = '0';
$field_values['field_options']['unique_msg'] = 'This value must be unique.';
$field_values['field_options']['autocom'] = '0';
$field_values['field_options']['dependent_fields'] = '';
$field_values['field_options']['custom_field'] = '';
$field_values['field_options']['post_field'] = 'post_title';
$field_values['field_options']['taxonomy'] = 'category';
$field_values['field_options']['exclude_cat'] = '0';
$frm_field->create( $field_values );

    
$field_values = apply_filters('frm_before_field_created', FrmFieldsHelper::setup_new_vars('text', $form_id));
$field_values['id'] = 86;
$field_values['field_key'] = 'tc_course_code2';
$field_values['name'] = 'Course Code';
$field_values['description'] = 'Unique Course ID Code';
$field_values['type'] = 'text';
$field_values['default_value'] = 'Course Code';
$field_values['options'] = '';
$field_values['required'] = '1';
$field_values['field_order'] = '2';
$field_values['field_options']['size'] = '';
$field_values['field_options']['max'] = '';
$field_values['field_options']['label'] = '';
$field_values['field_options']['blank'] = 'This field cannot be blank.';
$field_values['field_options']['required_indicator'] = '*';
$field_values['field_options']['invalid'] = '';
$field_values['field_options']['separate_value'] = '0';
$field_values['field_options']['clear_on_focus'] = '1';
$field_values['field_options']['default_blank'] = '1';
$field_values['field_options']['classes'] = '';
$field_values['field_options']['custom_html'] = '<div id=\"frm_field_[id]_container\" class=\"frm_form_field form-field [required_class][error_class]\">
    <label class=\"frm_primary_label\">[field_name]
        <span class=\"frm_required\">[required_label]</span>
    </label>
    [input]
    [if description]<div class=\"frm_description\">[description]</div>[/if description]
    [if error]<div class=\"frm_error\">[error]</div>[/if error]
</div>';
$field_values['field_options']['slide'] = '0';
$field_values['field_options']['form_select'] = '';
$field_values['field_options']['show_hide'] = 'show';
$field_values['field_options']['any_all'] = 'any';
$field_values['field_options']['align'] = 'block';
$field_values['field_options']['hide_field'] = 'a:0:{}';
$field_values['field_options']['hide_field_cond'] = 'a:1:{i:0;s:2:"==";}';
$field_values['field_options']['hide_opt'] = 'a:0:{}';
$field_values['field_options']['star'] = '0';
$field_values['field_options']['ftypes'] = 'a:0:{}';
$field_values['field_options']['data_type'] = '';
$field_values['field_options']['restrict'] = '0';
$field_values['field_options']['start_year'] = '2000';
$field_values['field_options']['end_year'] = '2020';
$field_values['field_options']['read_only'] = '0';
$field_values['field_options']['admin_only'] = '';
$field_values['field_options']['locale'] = '';
$field_values['field_options']['attach'] = '';
$field_values['field_options']['minnum'] = '0';
$field_values['field_options']['maxnum'] = '9999';
$field_values['field_options']['step'] = '1';
$field_values['field_options']['clock'] = '12';
$field_values['field_options']['start_time'] = '00:00';
$field_values['field_options']['end_time'] = '23:59';
$field_values['field_options']['unique'] = '0';
$field_values['field_options']['use_calc'] = '0';
$field_values['field_options']['calc'] = '';
$field_values['field_options']['duplication'] = '1';
$field_values['field_options']['rte'] = 'mce';
$field_values['field_options']['dyn_default_value'] = '';
$field_values['field_options']['multiple'] = '0';
$field_values['field_options']['unique_msg'] = 'This value must be unique.';
$field_values['field_options']['autocom'] = '0';
$field_values['field_options']['dependent_fields'] = '';
$field_values['field_options']['custom_field'] = 'course_code';
$field_values['field_options']['post_field'] = 'post_custom';
$field_values['field_options']['taxonomy'] = 'category';
$field_values['field_options']['exclude_cat'] = '0';
$frm_field->create( $field_values );

    
$field_values = apply_filters('frm_before_field_created', FrmFieldsHelper::setup_new_vars('text', $form_id));
$field_values['id'] = 87;
$field_values['field_key'] = 'tc_course_duration2';
$field_values['name'] = 'Course Duration';
$field_values['description'] = 'Duration of the Training Course';
$field_values['type'] = 'text';
$field_values['default_value'] = 'Course Duration';
$field_values['options'] = '';
$field_values['required'] = '1';
$field_values['field_order'] = '3';
$field_values['field_options']['size'] = '';
$field_values['field_options']['max'] = '';
$field_values['field_options']['label'] = '';
$field_values['field_options']['blank'] = 'This field cannot be blank.';
$field_values['field_options']['required_indicator'] = '*';
$field_values['field_options']['invalid'] = '';
$field_values['field_options']['separate_value'] = '0';
$field_values['field_options']['clear_on_focus'] = '1';
$field_values['field_options']['default_blank'] = '1';
$field_values['field_options']['classes'] = '';
$field_values['field_options']['custom_html'] = '<div id=\"frm_field_[id]_container\" class=\"frm_form_field form-field [required_class][error_class]\">
    <label class=\"frm_primary_label\">[field_name]
        <span class=\"frm_required\">[required_label]</span>
    </label>
    [input]
    [if description]<div class=\"frm_description\">[description]</div>[/if description]
    [if error]<div class=\"frm_error\">[error]</div>[/if error]
</div>';
$field_values['field_options']['slide'] = '0';
$field_values['field_options']['form_select'] = '';
$field_values['field_options']['show_hide'] = 'show';
$field_values['field_options']['any_all'] = 'any';
$field_values['field_options']['align'] = 'block';
$field_values['field_options']['hide_field'] = 'a:0:{}';
$field_values['field_options']['hide_field_cond'] = 'a:1:{i:0;s:2:"==";}';
$field_values['field_options']['hide_opt'] = 'a:0:{}';
$field_values['field_options']['star'] = '0';
$field_values['field_options']['ftypes'] = 'a:0:{}';
$field_values['field_options']['data_type'] = '';
$field_values['field_options']['restrict'] = '0';
$field_values['field_options']['start_year'] = '2000';
$field_values['field_options']['end_year'] = '2020';
$field_values['field_options']['read_only'] = '0';
$field_values['field_options']['admin_only'] = '';
$field_values['field_options']['locale'] = '';
$field_values['field_options']['attach'] = '';
$field_values['field_options']['minnum'] = '0';
$field_values['field_options']['maxnum'] = '9999';
$field_values['field_options']['step'] = '1';
$field_values['field_options']['clock'] = '12';
$field_values['field_options']['start_time'] = '00:00';
$field_values['field_options']['end_time'] = '23:59';
$field_values['field_options']['unique'] = '0';
$field_values['field_options']['use_calc'] = '0';
$field_values['field_options']['calc'] = '';
$field_values['field_options']['duplication'] = '1';
$field_values['field_options']['rte'] = 'mce';
$field_values['field_options']['dyn_default_value'] = '';
$field_values['field_options']['multiple'] = '0';
$field_values['field_options']['unique_msg'] = 'This value must be unique.';
$field_values['field_options']['autocom'] = '0';
$field_values['field_options']['dependent_fields'] = '';
$field_values['field_options']['custom_field'] = 'course_duration';
$field_values['field_options']['post_field'] = 'post_custom';
$field_values['field_options']['taxonomy'] = 'category';
$field_values['field_options']['exclude_cat'] = '0';
$frm_field->create( $field_values );

    
$field_values = apply_filters('frm_before_field_created', FrmFieldsHelper::setup_new_vars('text', $form_id));
$field_values['id'] = 88;
$field_values['field_key'] = 'tc_course_price2';
$field_values['name'] = 'Course Price';
$field_values['description'] = 'Price for the course (A$)';
$field_values['type'] = 'text';
$field_values['default_value'] = 'Course Price';
$field_values['options'] = '';
$field_values['required'] = '1';
$field_values['field_order'] = '4';
$field_values['field_options']['size'] = '';
$field_values['field_options']['max'] = '';
$field_values['field_options']['label'] = '';
$field_values['field_options']['blank'] = 'This field cannot be blank.';
$field_values['field_options']['required_indicator'] = '*';
$field_values['field_options']['invalid'] = '';
$field_values['field_options']['separate_value'] = '0';
$field_values['field_options']['clear_on_focus'] = '1';
$field_values['field_options']['default_blank'] = '1';
$field_values['field_options']['classes'] = '';
$field_values['field_options']['custom_html'] = '<div id=\"frm_field_[id]_container\" class=\"frm_form_field form-field [required_class][error_class]\">
    <label class=\"frm_primary_label\">[field_name]
        <span class=\"frm_required\">[required_label]</span>
    </label>
    [input]
    [if description]<div class=\"frm_description\">[description]</div>[/if description]
    [if error]<div class=\"frm_error\">[error]</div>[/if error]
</div>';
$field_values['field_options']['slide'] = '0';
$field_values['field_options']['form_select'] = '';
$field_values['field_options']['show_hide'] = 'show';
$field_values['field_options']['any_all'] = 'any';
$field_values['field_options']['align'] = 'block';
$field_values['field_options']['hide_field'] = 'a:0:{}';
$field_values['field_options']['hide_field_cond'] = 'a:1:{i:0;s:2:"==";}';
$field_values['field_options']['hide_opt'] = 'a:0:{}';
$field_values['field_options']['star'] = '0';
$field_values['field_options']['ftypes'] = 'a:0:{}';
$field_values['field_options']['data_type'] = '';
$field_values['field_options']['restrict'] = '0';
$field_values['field_options']['start_year'] = '2000';
$field_values['field_options']['end_year'] = '2020';
$field_values['field_options']['read_only'] = '0';
$field_values['field_options']['admin_only'] = '';
$field_values['field_options']['locale'] = '';
$field_values['field_options']['attach'] = '';
$field_values['field_options']['minnum'] = '0';
$field_values['field_options']['maxnum'] = '9999';
$field_values['field_options']['step'] = '1';
$field_values['field_options']['clock'] = '12';
$field_values['field_options']['start_time'] = '00:00';
$field_values['field_options']['end_time'] = '23:59';
$field_values['field_options']['unique'] = '0';
$field_values['field_options']['use_calc'] = '0';
$field_values['field_options']['calc'] = '';
$field_values['field_options']['duplication'] = '1';
$field_values['field_options']['rte'] = 'mce';
$field_values['field_options']['dyn_default_value'] = '';
$field_values['field_options']['multiple'] = '0';
$field_values['field_options']['unique_msg'] = 'This value must be unique.';
$field_values['field_options']['autocom'] = '0';
$field_values['field_options']['dependent_fields'] = '';
$field_values['field_options']['custom_field'] = 'course_price';
$field_values['field_options']['post_field'] = 'post_custom';
$field_values['field_options']['taxonomy'] = 'category';
$field_values['field_options']['exclude_cat'] = '0';
$frm_field->create( $field_values );

    
$field_values = apply_filters('frm_before_field_created', FrmFieldsHelper::setup_new_vars('text', $form_id));
$field_values['id'] = 89;
$field_values['field_key'] = 'tc_course_location2';
$field_values['name'] = 'Course Venue Location';
$field_values['description'] = 'Location of the Training Course';
$field_values['type'] = 'text';
$field_values['default_value'] = 'Course Venue Location';
$field_values['options'] = '';
$field_values['required'] = '1';
$field_values['field_order'] = '5';
$field_values['field_options']['size'] = '';
$field_values['field_options']['max'] = '';
$field_values['field_options']['label'] = '';
$field_values['field_options']['blank'] = 'This field cannot be blank.';
$field_values['field_options']['required_indicator'] = '*';
$field_values['field_options']['invalid'] = '';
$field_values['field_options']['separate_value'] = '0';
$field_values['field_options']['clear_on_focus'] = '1';
$field_values['field_options']['default_blank'] = '1';
$field_values['field_options']['classes'] = '';
$field_values['field_options']['custom_html'] = '<div id=\"frm_field_[id]_container\" class=\"frm_form_field form-field [required_class][error_class]\">
    <label class=\"frm_primary_label\">[field_name]
        <span class=\"frm_required\">[required_label]</span>
    </label>
    [input]
    [if description]<div class=\"frm_description\">[description]</div>[/if description]
    [if error]<div class=\"frm_error\">[error]</div>[/if error]
</div>';
$field_values['field_options']['slide'] = '0';
$field_values['field_options']['form_select'] = '';
$field_values['field_options']['show_hide'] = 'show';
$field_values['field_options']['any_all'] = 'any';
$field_values['field_options']['align'] = 'block';
$field_values['field_options']['hide_field'] = 'a:0:{}';
$field_values['field_options']['hide_field_cond'] = 'a:1:{i:0;s:2:"==";}';
$field_values['field_options']['hide_opt'] = 'a:0:{}';
$field_values['field_options']['star'] = '0';
$field_values['field_options']['ftypes'] = 'a:0:{}';
$field_values['field_options']['data_type'] = '';
$field_values['field_options']['restrict'] = '0';
$field_values['field_options']['start_year'] = '2000';
$field_values['field_options']['end_year'] = '2020';
$field_values['field_options']['read_only'] = '0';
$field_values['field_options']['admin_only'] = '';
$field_values['field_options']['locale'] = '';
$field_values['field_options']['attach'] = '';
$field_values['field_options']['minnum'] = '0';
$field_values['field_options']['maxnum'] = '9999';
$field_values['field_options']['step'] = '1';
$field_values['field_options']['clock'] = '12';
$field_values['field_options']['start_time'] = '00:00';
$field_values['field_options']['end_time'] = '23:59';
$field_values['field_options']['unique'] = '0';
$field_values['field_options']['use_calc'] = '0';
$field_values['field_options']['calc'] = '';
$field_values['field_options']['duplication'] = '1';
$field_values['field_options']['rte'] = 'mce';
$field_values['field_options']['dyn_default_value'] = '';
$field_values['field_options']['multiple'] = '0';
$field_values['field_options']['unique_msg'] = 'This value must be unique.';
$field_values['field_options']['autocom'] = '0';
$field_values['field_options']['dependent_fields'] = '';
$field_values['field_options']['custom_field'] = 'course_location';
$field_values['field_options']['post_field'] = 'post_custom';
$field_values['field_options']['taxonomy'] = 'category';
$field_values['field_options']['exclude_cat'] = '0';
$frm_field->create( $field_values );

    
$field_values = apply_filters('frm_before_field_created', FrmFieldsHelper::setup_new_vars('time', $form_id));
$field_values['id'] = 90;
$field_values['field_key'] = 'tc_course_starttime2';
$field_values['name'] = 'Course Start Time';
$field_values['description'] = 'Starting Time of the Course';
$field_values['type'] = 'time';
$field_values['default_value'] = '';
$field_values['options'] = '';
$field_values['required'] = '1';
$field_values['field_order'] = '6';
$field_values['field_options']['size'] = '1';
$field_values['field_options']['max'] = '';
$field_values['field_options']['label'] = '';
$field_values['field_options']['blank'] = 'This field cannot be blank.';
$field_values['field_options']['required_indicator'] = '*';
$field_values['field_options']['invalid'] = '';
$field_values['field_options']['separate_value'] = '0';
$field_values['field_options']['clear_on_focus'] = '0';
$field_values['field_options']['default_blank'] = '0';
$field_values['field_options']['classes'] = '';
$field_values['field_options']['custom_html'] = '<div id=\"frm_field_[id]_container\" class=\"frm_form_field form-field [required_class][error_class]\">
    <label class=\"frm_primary_label\">[field_name]
        <span class=\"frm_required\">[required_label]</span>
    </label>
    [input]
    [if description]<div class=\"frm_description\">[description]</div>[/if description]
    [if error]<div class=\"frm_error\">[error]</div>[/if error]
</div>';
$field_values['field_options']['slide'] = '0';
$field_values['field_options']['form_select'] = '';
$field_values['field_options']['show_hide'] = 'show';
$field_values['field_options']['any_all'] = 'any';
$field_values['field_options']['align'] = 'block';
$field_values['field_options']['hide_field'] = 'a:0:{}';
$field_values['field_options']['hide_field_cond'] = 'a:1:{i:0;s:2:"==";}';
$field_values['field_options']['hide_opt'] = 'a:0:{}';
$field_values['field_options']['star'] = '0';
$field_values['field_options']['ftypes'] = 'a:0:{}';
$field_values['field_options']['data_type'] = '';
$field_values['field_options']['restrict'] = '0';
$field_values['field_options']['start_year'] = '2000';
$field_values['field_options']['end_year'] = '2020';
$field_values['field_options']['read_only'] = '0';
$field_values['field_options']['admin_only'] = '';
$field_values['field_options']['locale'] = '';
$field_values['field_options']['attach'] = '';
$field_values['field_options']['minnum'] = '0';
$field_values['field_options']['maxnum'] = '9999';
$field_values['field_options']['step'] = '30';
$field_values['field_options']['clock'] = '12';
$field_values['field_options']['start_time'] = '00:00';
$field_values['field_options']['end_time'] = '23:30';
$field_values['field_options']['unique'] = '0';
$field_values['field_options']['use_calc'] = '0';
$field_values['field_options']['calc'] = '';
$field_values['field_options']['duplication'] = '1';
$field_values['field_options']['rte'] = 'mce';
$field_values['field_options']['dyn_default_value'] = '';
$field_values['field_options']['multiple'] = '0';
$field_values['field_options']['unique_msg'] = 'This value must be unique.';
$field_values['field_options']['autocom'] = '0';
$field_values['field_options']['dependent_fields'] = '';
$field_values['field_options']['custom_field'] = 'course_starttime';
$field_values['field_options']['post_field'] = 'post_custom';
$field_values['field_options']['taxonomy'] = 'category';
$field_values['field_options']['exclude_cat'] = '0';
$frm_field->create( $field_values );

    
$field_values = apply_filters('frm_before_field_created', FrmFieldsHelper::setup_new_vars('date', $form_id));
$field_values['id'] = 91;
$field_values['field_key'] = 'tc_course_startdate2';
$field_values['name'] = 'Course Start Date';
$field_values['description'] = 'Starting Date of the Course';
$field_values['type'] = 'date';
$field_values['default_value'] = '';
$field_values['options'] = '';
$field_values['required'] = '1';
$field_values['field_order'] = '7';
$field_values['field_options']['size'] = '10';
$field_values['field_options']['max'] = '10';
$field_values['field_options']['label'] = '';
$field_values['field_options']['blank'] = 'This field cannot be blank.';
$field_values['field_options']['required_indicator'] = '*';
$field_values['field_options']['invalid'] = 'Course Start Date is invalid';
$field_values['field_options']['separate_value'] = '0';
$field_values['field_options']['clear_on_focus'] = '0';
$field_values['field_options']['default_blank'] = '0';
$field_values['field_options']['classes'] = '';
$field_values['field_options']['custom_html'] = '<div id=\"frm_field_[id]_container\" class=\"frm_form_field form-field [required_class][error_class]\">
    <label class=\"frm_primary_label\">[field_name]
        <span class=\"frm_required\">[required_label]</span>
    </label>
    [input]
    [if description]<div class=\"frm_description\">[description]</div>[/if description]
    [if error]<div class=\"frm_error\">[error]</div>[/if error]
</div>';
$field_values['field_options']['slide'] = '0';
$field_values['field_options']['form_select'] = '';
$field_values['field_options']['show_hide'] = 'show';
$field_values['field_options']['any_all'] = 'any';
$field_values['field_options']['align'] = 'block';
$field_values['field_options']['hide_field'] = 'a:0:{}';
$field_values['field_options']['hide_field_cond'] = 'a:1:{i:0;s:2:"==";}';
$field_values['field_options']['hide_opt'] = 'a:0:{}';
$field_values['field_options']['star'] = '0';
$field_values['field_options']['ftypes'] = 'a:0:{}';
$field_values['field_options']['data_type'] = '';
$field_values['field_options']['restrict'] = '0';
$field_values['field_options']['start_year'] = '2000';
$field_values['field_options']['end_year'] = '2020';
$field_values['field_options']['read_only'] = '0';
$field_values['field_options']['admin_only'] = '';
$field_values['field_options']['locale'] = '';
$field_values['field_options']['attach'] = '';
$field_values['field_options']['minnum'] = '0';
$field_values['field_options']['maxnum'] = '9999';
$field_values['field_options']['step'] = '1';
$field_values['field_options']['clock'] = '12';
$field_values['field_options']['start_time'] = '00:00';
$field_values['field_options']['end_time'] = '23:59';
$field_values['field_options']['unique'] = '0';
$field_values['field_options']['use_calc'] = '0';
$field_values['field_options']['calc'] = '';
$field_values['field_options']['duplication'] = '1';
$field_values['field_options']['rte'] = 'mce';
$field_values['field_options']['dyn_default_value'] = '';
$field_values['field_options']['multiple'] = '0';
$field_values['field_options']['unique_msg'] = 'This value must be unique.';
$field_values['field_options']['autocom'] = '0';
$field_values['field_options']['dependent_fields'] = '';
$field_values['field_options']['custom_field'] = 'course_startdate';
$field_values['field_options']['post_field'] = 'post_custom';
$field_values['field_options']['taxonomy'] = 'category';
$field_values['field_options']['exclude_cat'] = '0';
$frm_field->create( $field_values );

    
$field_values = apply_filters('frm_before_field_created', FrmFieldsHelper::setup_new_vars('time', $form_id));
$field_values['id'] = 92;
$field_values['field_key'] = 'tc_course_endtime3';
$field_values['name'] = 'Course End Time';
$field_values['description'] = 'Ending Time of the Course';
$field_values['type'] = 'time';
$field_values['default_value'] = '';
$field_values['options'] = '';
$field_values['required'] = '1';
$field_values['field_order'] = '8';
$field_values['field_options']['size'] = '1';
$field_values['field_options']['max'] = '';
$field_values['field_options']['label'] = '';
$field_values['field_options']['blank'] = 'This field cannot be blank.';
$field_values['field_options']['required_indicator'] = '*';
$field_values['field_options']['invalid'] = '';
$field_values['field_options']['separate_value'] = '0';
$field_values['field_options']['clear_on_focus'] = '0';
$field_values['field_options']['default_blank'] = '0';
$field_values['field_options']['classes'] = '';
$field_values['field_options']['custom_html'] = '<div id=\"frm_field_[id]_container\" class=\"frm_form_field form-field [required_class][error_class]\">
    <label class=\"frm_primary_label\">[field_name]
        <span class=\"frm_required\">[required_label]</span>
    </label>
    [input]
    [if description]<div class=\"frm_description\">[description]</div>[/if description]
    [if error]<div class=\"frm_error\">[error]</div>[/if error]
</div>';
$field_values['field_options']['slide'] = '0';
$field_values['field_options']['form_select'] = '';
$field_values['field_options']['show_hide'] = 'show';
$field_values['field_options']['any_all'] = 'any';
$field_values['field_options']['align'] = 'block';
$field_values['field_options']['hide_field'] = 'a:0:{}';
$field_values['field_options']['hide_field_cond'] = 'a:1:{i:0;s:2:"==";}';
$field_values['field_options']['hide_opt'] = 'a:0:{}';
$field_values['field_options']['star'] = '0';
$field_values['field_options']['ftypes'] = 'a:0:{}';
$field_values['field_options']['data_type'] = '';
$field_values['field_options']['restrict'] = '0';
$field_values['field_options']['start_year'] = '2000';
$field_values['field_options']['end_year'] = '2020';
$field_values['field_options']['read_only'] = '0';
$field_values['field_options']['admin_only'] = '';
$field_values['field_options']['locale'] = '';
$field_values['field_options']['attach'] = '';
$field_values['field_options']['minnum'] = '0';
$field_values['field_options']['maxnum'] = '9999';
$field_values['field_options']['step'] = '30';
$field_values['field_options']['clock'] = '12';
$field_values['field_options']['start_time'] = '00:00';
$field_values['field_options']['end_time'] = '23:30';
$field_values['field_options']['unique'] = '0';
$field_values['field_options']['use_calc'] = '0';
$field_values['field_options']['calc'] = '';
$field_values['field_options']['duplication'] = '1';
$field_values['field_options']['rte'] = 'mce';
$field_values['field_options']['dyn_default_value'] = '';
$field_values['field_options']['multiple'] = '0';
$field_values['field_options']['unique_msg'] = 'This value must be unique.';
$field_values['field_options']['autocom'] = '0';
$field_values['field_options']['dependent_fields'] = '';
$field_values['field_options']['custom_field'] = 'course_endtime';
$field_values['field_options']['post_field'] = 'post_custom';
$field_values['field_options']['taxonomy'] = 'category';
$field_values['field_options']['exclude_cat'] = '0';
$frm_field->create( $field_values );

    
$field_values = apply_filters('frm_before_field_created', FrmFieldsHelper::setup_new_vars('date', $form_id));
$field_values['id'] = 93;
$field_values['field_key'] = 'tc_course_endtime22';
$field_values['name'] = 'Course End Date';
$field_values['description'] = 'Ending Date of the Course';
$field_values['type'] = 'date';
$field_values['default_value'] = '';
$field_values['options'] = '';
$field_values['required'] = '1';
$field_values['field_order'] = '9';
$field_values['field_options']['size'] = '10';
$field_values['field_options']['max'] = '10';
$field_values['field_options']['label'] = '';
$field_values['field_options']['blank'] = 'This field cannot be blank.';
$field_values['field_options']['required_indicator'] = '*';
$field_values['field_options']['invalid'] = 'Course End Date is invalid';
$field_values['field_options']['separate_value'] = '0';
$field_values['field_options']['clear_on_focus'] = '0';
$field_values['field_options']['default_blank'] = '0';
$field_values['field_options']['classes'] = '';
$field_values['field_options']['custom_html'] = '<div id=\"frm_field_[id]_container\" class=\"frm_form_field form-field [required_class][error_class]\">
    <label class=\"frm_primary_label\">[field_name]
        <span class=\"frm_required\">[required_label]</span>
    </label>
    [input]
    [if description]<div class=\"frm_description\">[description]</div>[/if description]
    [if error]<div class=\"frm_error\">[error]</div>[/if error]
</div>';
$field_values['field_options']['slide'] = '0';
$field_values['field_options']['form_select'] = '';
$field_values['field_options']['show_hide'] = 'show';
$field_values['field_options']['any_all'] = 'any';
$field_values['field_options']['align'] = 'block';
$field_values['field_options']['hide_field'] = 'a:0:{}';
$field_values['field_options']['hide_field_cond'] = 'a:1:{i:0;s:2:"==";}';
$field_values['field_options']['hide_opt'] = 'a:0:{}';
$field_values['field_options']['star'] = '0';
$field_values['field_options']['ftypes'] = 'a:0:{}';
$field_values['field_options']['data_type'] = '';
$field_values['field_options']['restrict'] = '0';
$field_values['field_options']['start_year'] = '2000';
$field_values['field_options']['end_year'] = '2020';
$field_values['field_options']['read_only'] = '0';
$field_values['field_options']['admin_only'] = '';
$field_values['field_options']['locale'] = '';
$field_values['field_options']['attach'] = '';
$field_values['field_options']['minnum'] = '0';
$field_values['field_options']['maxnum'] = '9999';
$field_values['field_options']['step'] = '1';
$field_values['field_options']['clock'] = '12';
$field_values['field_options']['start_time'] = '00:00';
$field_values['field_options']['end_time'] = '23:59';
$field_values['field_options']['unique'] = '0';
$field_values['field_options']['use_calc'] = '0';
$field_values['field_options']['calc'] = '';
$field_values['field_options']['duplication'] = '1';
$field_values['field_options']['rte'] = 'mce';
$field_values['field_options']['dyn_default_value'] = '';
$field_values['field_options']['multiple'] = '0';
$field_values['field_options']['unique_msg'] = 'This value must be unique.';
$field_values['field_options']['autocom'] = '0';
$field_values['field_options']['dependent_fields'] = '';
$field_values['field_options']['custom_field'] = 'course_enddate';
$field_values['field_options']['post_field'] = 'post_custom';
$field_values['field_options']['taxonomy'] = 'category';
$field_values['field_options']['exclude_cat'] = '0';
$frm_field->create( $field_values );

    
$field_values = apply_filters('frm_before_field_created', FrmFieldsHelper::setup_new_vars('checkbox', $form_id));
$field_values['id'] = 94;
$field_values['field_key'] = 'tc_course_delivery2';
$field_values['name'] = 'Training Course Delivery Method';
$field_values['description'] = 'How the course will be delivered';
$field_values['type'] = 'checkbox';
$field_values['default_value'] = '';
$field_values['options'] = 'a:6:{i:0;s:14:"Instructor Led";i:1;s:19:"Instructor Assisted";i:2;s:22:"Virtual Instructor Led";i:3;s:7:"Webinar";i:4;s:18:"Private One-on-One";i:5;s:26:"Virtual Private One-on-One";}';
$field_values['required'] = '1';
$field_values['field_order'] = '10';
$field_values['field_options']['size'] = '';
$field_values['field_options']['max'] = '';
$field_values['field_options']['label'] = '';
$field_values['field_options']['blank'] = 'This field cannot be blank.';
$field_values['field_options']['required_indicator'] = '*';
$field_values['field_options']['invalid'] = '';
$field_values['field_options']['separate_value'] = '0';
$field_values['field_options']['clear_on_focus'] = '0';
$field_values['field_options']['default_blank'] = '0';
$field_values['field_options']['classes'] = '';
$field_values['field_options']['custom_html'] = '<div id=\"frm_field_[id]_container\" class=\"frm_form_field form-field [required_class][error_class]\">
    <label class=\"frm_primary_label\">[field_name]
        <span class=\"frm_required\">[required_label]</span>
    </label>
    <div class=\"frm_opt_container\">[input]</div>
    [if description]<div class=\"frm_description\">[description]</div>[/if description]
    [if error]<div class=\"frm_error\">[error]</div>[/if error]
</div>';
$field_values['field_options']['slide'] = '0';
$field_values['field_options']['form_select'] = '';
$field_values['field_options']['show_hide'] = 'show';
$field_values['field_options']['any_all'] = 'any';
$field_values['field_options']['align'] = 'block';
$field_values['field_options']['hide_field'] = 'a:0:{}';
$field_values['field_options']['hide_field_cond'] = 'a:1:{i:0;s:2:"==";}';
$field_values['field_options']['hide_opt'] = 'a:0:{}';
$field_values['field_options']['star'] = '0';
$field_values['field_options']['ftypes'] = 'a:0:{}';
$field_values['field_options']['data_type'] = '';
$field_values['field_options']['restrict'] = '0';
$field_values['field_options']['start_year'] = '2000';
$field_values['field_options']['end_year'] = '2020';
$field_values['field_options']['read_only'] = '0';
$field_values['field_options']['admin_only'] = '';
$field_values['field_options']['locale'] = '';
$field_values['field_options']['attach'] = '';
$field_values['field_options']['minnum'] = '0';
$field_values['field_options']['maxnum'] = '9999';
$field_values['field_options']['step'] = '1';
$field_values['field_options']['clock'] = '12';
$field_values['field_options']['start_time'] = '00:00';
$field_values['field_options']['end_time'] = '23:59';
$field_values['field_options']['unique'] = '0';
$field_values['field_options']['use_calc'] = '0';
$field_values['field_options']['calc'] = '';
$field_values['field_options']['duplication'] = '1';
$field_values['field_options']['rte'] = 'mce';
$field_values['field_options']['dyn_default_value'] = '';
$field_values['field_options']['multiple'] = '0';
$field_values['field_options']['unique_msg'] = 'This value must be unique.';
$field_values['field_options']['autocom'] = '0';
$field_values['field_options']['dependent_fields'] = '';
$field_values['field_options']['custom_field'] = 'course_delivery';
$field_values['field_options']['post_field'] = 'post_custom';
$field_values['field_options']['taxonomy'] = 'category';
$field_values['field_options']['exclude_cat'] = '0';
$frm_field->create( $field_values );

    
$field_values = apply_filters('frm_before_field_created', FrmFieldsHelper::setup_new_vars('textarea', $form_id));
$field_values['id'] = 95;
$field_values['field_key'] = 'tc_course_overview2';
$field_values['name'] = 'Course Overview';
$field_values['description'] = 'overview of the Training Course';
$field_values['type'] = 'textarea';
$field_values['default_value'] = '';
$field_values['options'] = '';
$field_values['required'] = '1';
$field_values['field_order'] = '11';
$field_values['field_options']['size'] = '';
$field_values['field_options']['max'] = '5';
$field_values['field_options']['label'] = '';
$field_values['field_options']['blank'] = 'This field cannot be blank.';
$field_values['field_options']['required_indicator'] = '*';
$field_values['field_options']['invalid'] = '';
$field_values['field_options']['separate_value'] = '0';
$field_values['field_options']['clear_on_focus'] = '0';
$field_values['field_options']['default_blank'] = '0';
$field_values['field_options']['classes'] = '';
$field_values['field_options']['custom_html'] = '<div id=\"frm_field_[id]_container\" class=\"frm_form_field form-field [required_class][error_class]\">
    <label class=\"frm_primary_label\">[field_name]
        <span class=\"frm_required\">[required_label]</span>
    </label>
    [input]
    [if description]<div class=\"frm_description\">[description]</div>[/if description]
    [if error]<div class=\"frm_error\">[error]</div>[/if error]
</div>';
$field_values['field_options']['slide'] = '0';
$field_values['field_options']['form_select'] = '';
$field_values['field_options']['show_hide'] = 'show';
$field_values['field_options']['any_all'] = 'any';
$field_values['field_options']['align'] = 'block';
$field_values['field_options']['hide_field'] = 'a:0:{}';
$field_values['field_options']['hide_field_cond'] = 'a:1:{i:0;s:2:"==";}';
$field_values['field_options']['hide_opt'] = 'a:0:{}';
$field_values['field_options']['star'] = '0';
$field_values['field_options']['ftypes'] = 'a:0:{}';
$field_values['field_options']['data_type'] = '';
$field_values['field_options']['restrict'] = '0';
$field_values['field_options']['start_year'] = '2000';
$field_values['field_options']['end_year'] = '2020';
$field_values['field_options']['read_only'] = '0';
$field_values['field_options']['admin_only'] = '';
$field_values['field_options']['locale'] = '';
$field_values['field_options']['attach'] = '';
$field_values['field_options']['minnum'] = '0';
$field_values['field_options']['maxnum'] = '9999';
$field_values['field_options']['step'] = '1';
$field_values['field_options']['clock'] = '12';
$field_values['field_options']['start_time'] = '00:00';
$field_values['field_options']['end_time'] = '23:59';
$field_values['field_options']['unique'] = '0';
$field_values['field_options']['use_calc'] = '0';
$field_values['field_options']['calc'] = '';
$field_values['field_options']['duplication'] = '1';
$field_values['field_options']['rte'] = 'mce';
$field_values['field_options']['dyn_default_value'] = '';
$field_values['field_options']['multiple'] = '0';
$field_values['field_options']['unique_msg'] = 'This value must be unique.';
$field_values['field_options']['autocom'] = '0';
$field_values['field_options']['dependent_fields'] = '';
$field_values['field_options']['custom_field'] = 'course_overview';
$field_values['field_options']['post_field'] = 'post_custom';
$field_values['field_options']['taxonomy'] = 'category';
$field_values['field_options']['exclude_cat'] = '0';
$frm_field->create( $field_values );

    
$field_values = apply_filters('frm_before_field_created', FrmFieldsHelper::setup_new_vars('text', $form_id));
$field_values['id'] = 96;
$field_values['field_key'] = 'tc_course_requirements2';
$field_values['name'] = 'Course Pre-Requirements';
$field_values['description'] = 'Requirements of the Training Course';
$field_values['type'] = 'text';
$field_values['default_value'] = '';
$field_values['options'] = '';
$field_values['required'] = '1';
$field_values['field_order'] = '12';
$field_values['field_options']['size'] = '';
$field_values['field_options']['max'] = '';
$field_values['field_options']['label'] = '';
$field_values['field_options']['blank'] = 'This field cannot be blank.';
$field_values['field_options']['required_indicator'] = '*';
$field_values['field_options']['invalid'] = '';
$field_values['field_options']['separate_value'] = '0';
$field_values['field_options']['clear_on_focus'] = '0';
$field_values['field_options']['default_blank'] = '0';
$field_values['field_options']['classes'] = '';
$field_values['field_options']['custom_html'] = '<div id=\"frm_field_[id]_container\" class=\"frm_form_field form-field [required_class][error_class]\">
    <label class=\"frm_primary_label\">[field_name]
        <span class=\"frm_required\">[required_label]</span>
    </label>
    [input]
    [if description]<div class=\"frm_description\">[description]</div>[/if description]
    [if error]<div class=\"frm_error\">[error]</div>[/if error]
</div>';
$field_values['field_options']['slide'] = '0';
$field_values['field_options']['form_select'] = '';
$field_values['field_options']['show_hide'] = 'show';
$field_values['field_options']['any_all'] = 'any';
$field_values['field_options']['align'] = 'block';
$field_values['field_options']['hide_field'] = 'a:0:{}';
$field_values['field_options']['hide_field_cond'] = 'a:1:{i:0;s:2:"==";}';
$field_values['field_options']['hide_opt'] = 'a:0:{}';
$field_values['field_options']['star'] = '0';
$field_values['field_options']['ftypes'] = 'a:0:{}';
$field_values['field_options']['data_type'] = '';
$field_values['field_options']['restrict'] = '0';
$field_values['field_options']['start_year'] = '2000';
$field_values['field_options']['end_year'] = '2020';
$field_values['field_options']['read_only'] = '0';
$field_values['field_options']['admin_only'] = '';
$field_values['field_options']['locale'] = '';
$field_values['field_options']['attach'] = '';
$field_values['field_options']['minnum'] = '0';
$field_values['field_options']['maxnum'] = '9999';
$field_values['field_options']['step'] = '1';
$field_values['field_options']['clock'] = '12';
$field_values['field_options']['start_time'] = '00:00';
$field_values['field_options']['end_time'] = '23:59';
$field_values['field_options']['unique'] = '0';
$field_values['field_options']['use_calc'] = '0';
$field_values['field_options']['calc'] = '';
$field_values['field_options']['duplication'] = '1';
$field_values['field_options']['rte'] = 'mce';
$field_values['field_options']['dyn_default_value'] = '';
$field_values['field_options']['multiple'] = '0';
$field_values['field_options']['unique_msg'] = 'This value must be unique.';
$field_values['field_options']['autocom'] = '0';
$field_values['field_options']['dependent_fields'] = '';
$field_values['field_options']['custom_field'] = 'course_requirements';
$field_values['field_options']['post_field'] = 'post_custom';
$field_values['field_options']['taxonomy'] = 'category';
$field_values['field_options']['exclude_cat'] = '0';
$frm_field->create( $field_values );

    
$field_values = apply_filters('frm_before_field_created', FrmFieldsHelper::setup_new_vars('text', $form_id));
$field_values['id'] = 97;
$field_values['field_key'] = 'tc_course_accreditations2';
$field_values['name'] = 'Course Accreditations';
$field_values['description'] = '';
$field_values['type'] = 'text';
$field_values['default_value'] = '';
$field_values['options'] = '';
$field_values['required'] = '1';
$field_values['field_order'] = '13';
$field_values['field_options']['size'] = '';
$field_values['field_options']['max'] = '';
$field_values['field_options']['label'] = '';
$field_values['field_options']['blank'] = 'This field cannot be blank.';
$field_values['field_options']['required_indicator'] = '*';
$field_values['field_options']['invalid'] = '';
$field_values['field_options']['separate_value'] = '0';
$field_values['field_options']['clear_on_focus'] = '0';
$field_values['field_options']['default_blank'] = '0';
$field_values['field_options']['classes'] = '';
$field_values['field_options']['custom_html'] = '<div id=\"frm_field_[id]_container\" class=\"frm_form_field form-field [required_class][error_class]\">
    <label class=\"frm_primary_label\">[field_name]
        <span class=\"frm_required\">[required_label]</span>
    </label>
    [input]
    [if description]<div class=\"frm_description\">[description]</div>[/if description]
    [if error]<div class=\"frm_error\">[error]</div>[/if error]
</div>';
$field_values['field_options']['slide'] = '0';
$field_values['field_options']['form_select'] = '';
$field_values['field_options']['show_hide'] = 'show';
$field_values['field_options']['any_all'] = 'any';
$field_values['field_options']['align'] = 'block';
$field_values['field_options']['hide_field'] = 'a:0:{}';
$field_values['field_options']['hide_field_cond'] = 'a:1:{i:0;s:2:"==";}';
$field_values['field_options']['hide_opt'] = 'a:0:{}';
$field_values['field_options']['star'] = '0';
$field_values['field_options']['ftypes'] = 'a:0:{}';
$field_values['field_options']['data_type'] = '';
$field_values['field_options']['restrict'] = '0';
$field_values['field_options']['start_year'] = '2000';
$field_values['field_options']['end_year'] = '2020';
$field_values['field_options']['read_only'] = '0';
$field_values['field_options']['admin_only'] = '';
$field_values['field_options']['locale'] = '';
$field_values['field_options']['attach'] = '';
$field_values['field_options']['minnum'] = '0';
$field_values['field_options']['maxnum'] = '9999';
$field_values['field_options']['step'] = '1';
$field_values['field_options']['clock'] = '12';
$field_values['field_options']['start_time'] = '00:00';
$field_values['field_options']['end_time'] = '23:59';
$field_values['field_options']['unique'] = '0';
$field_values['field_options']['use_calc'] = '0';
$field_values['field_options']['calc'] = '';
$field_values['field_options']['duplication'] = '1';
$field_values['field_options']['rte'] = 'mce';
$field_values['field_options']['dyn_default_value'] = '';
$field_values['field_options']['multiple'] = '0';
$field_values['field_options']['unique_msg'] = 'This value must be unique.';
$field_values['field_options']['autocom'] = '0';
$field_values['field_options']['dependent_fields'] = '';
$field_values['field_options']['custom_field'] = 'course_accreditations';
$field_values['field_options']['post_field'] = 'post_custom';
$field_values['field_options']['taxonomy'] = 'category';
$field_values['field_options']['exclude_cat'] = '0';
$frm_field->create( $field_values );

    
$field_values = apply_filters('frm_before_field_created', FrmFieldsHelper::setup_new_vars('rte', $form_id));
$field_values['id'] = 100;
$field_values['field_key'] = 'tc_course_information2';
$field_values['name'] = 'Course Information';
$field_values['description'] = 'Full Information on the Training Course';
$field_values['type'] = 'rte';
$field_values['default_value'] = '';
$field_values['options'] = '';
$field_values['required'] = '1';
$field_values['field_order'] = '14';
$field_values['field_options']['size'] = '';
$field_values['field_options']['max'] = '7';
$field_values['field_options']['label'] = '';
$field_values['field_options']['blank'] = 'This field cannot be blank.';
$field_values['field_options']['required_indicator'] = '*';
$field_values['field_options']['invalid'] = '';
$field_values['field_options']['separate_value'] = '0';
$field_values['field_options']['clear_on_focus'] = '0';
$field_values['field_options']['default_blank'] = '0';
$field_values['field_options']['classes'] = '';
$field_values['field_options']['custom_html'] = '<div id=\"frm_field_[id]_container\" class=\"frm_form_field form-field [required_class][error_class]\">
    <label class=\"frm_primary_label\">[field_name]
        <span class=\"frm_required\">[required_label]</span>
    </label>
    [input]
    [if description]<div class=\"frm_description\">[description]</div>[/if description]
    [if error]<div class=\"frm_error\">[error]</div>[/if error]
</div>';
$field_values['field_options']['slide'] = '0';
$field_values['field_options']['form_select'] = '';
$field_values['field_options']['show_hide'] = 'show';
$field_values['field_options']['any_all'] = 'any';
$field_values['field_options']['align'] = 'block';
$field_values['field_options']['hide_field'] = 'a:0:{}';
$field_values['field_options']['hide_field_cond'] = 'a:1:{i:0;s:2:"==";}';
$field_values['field_options']['hide_opt'] = 'a:0:{}';
$field_values['field_options']['star'] = '0';
$field_values['field_options']['ftypes'] = 'a:0:{}';
$field_values['field_options']['data_type'] = '';
$field_values['field_options']['restrict'] = '0';
$field_values['field_options']['start_year'] = '2000';
$field_values['field_options']['end_year'] = '2020';
$field_values['field_options']['read_only'] = '0';
$field_values['field_options']['admin_only'] = '';
$field_values['field_options']['locale'] = '';
$field_values['field_options']['attach'] = '';
$field_values['field_options']['minnum'] = '0';
$field_values['field_options']['maxnum'] = '9999';
$field_values['field_options']['step'] = '1';
$field_values['field_options']['clock'] = '12';
$field_values['field_options']['start_time'] = '00:00';
$field_values['field_options']['end_time'] = '23:59';
$field_values['field_options']['unique'] = '0';
$field_values['field_options']['use_calc'] = '0';
$field_values['field_options']['calc'] = '';
$field_values['field_options']['duplication'] = '1';
$field_values['field_options']['rte'] = 'mce';
$field_values['field_options']['dyn_default_value'] = '';
$field_values['field_options']['multiple'] = '0';
$field_values['field_options']['unique_msg'] = 'This value must be unique.';
$field_values['field_options']['autocom'] = '0';
$field_values['field_options']['dependent_fields'] = '';
$field_values['field_options']['custom_field'] = 'course_information';
$field_values['field_options']['post_field'] = 'post_custom';
$field_values['field_options']['taxonomy'] = 'category';
$field_values['field_options']['exclude_cat'] = '0';
$frm_field->create( $field_values );

    
$field_values = apply_filters('frm_before_field_created', FrmFieldsHelper::setup_new_vars('text', $form_id));
$field_values['id'] = 101;
$field_values['field_key'] = 'tc_course_promoprice2';
$field_values['name'] = 'Course Promotion Price';
$field_values['description'] = '';
$field_values['type'] = 'text';
$field_values['default_value'] = '';
$field_values['options'] = '';
$field_values['required'] = '0';
$field_values['field_order'] = '15';
$field_values['field_options']['size'] = '';
$field_values['field_options']['max'] = '';
$field_values['field_options']['label'] = '';
$field_values['field_options']['blank'] = 'This field cannot be blank.';
$field_values['field_options']['required_indicator'] = '*';
$field_values['field_options']['invalid'] = '';
$field_values['field_options']['separate_value'] = '0';
$field_values['field_options']['clear_on_focus'] = '0';
$field_values['field_options']['default_blank'] = '0';
$field_values['field_options']['classes'] = '';
$field_values['field_options']['custom_html'] = '<div id=\"frm_field_[id]_container\" class=\"frm_form_field form-field [required_class][error_class]\">
    <label class=\"frm_primary_label\">[field_name]
        <span class=\"frm_required\">[required_label]</span>
    </label>
    [input]
    [if description]<div class=\"frm_description\">[description]</div>[/if description]
    [if error]<div class=\"frm_error\">[error]</div>[/if error]
</div>';
$field_values['field_options']['slide'] = '0';
$field_values['field_options']['form_select'] = '';
$field_values['field_options']['show_hide'] = 'show';
$field_values['field_options']['any_all'] = 'any';
$field_values['field_options']['align'] = 'block';
$field_values['field_options']['hide_field'] = 'a:0:{}';
$field_values['field_options']['hide_field_cond'] = 'a:1:{i:0;s:2:"==";}';
$field_values['field_options']['hide_opt'] = 'a:0:{}';
$field_values['field_options']['star'] = '0';
$field_values['field_options']['ftypes'] = 'a:0:{}';
$field_values['field_options']['data_type'] = '';
$field_values['field_options']['restrict'] = '0';
$field_values['field_options']['start_year'] = '2000';
$field_values['field_options']['end_year'] = '2020';
$field_values['field_options']['read_only'] = '0';
$field_values['field_options']['admin_only'] = '';
$field_values['field_options']['locale'] = '';
$field_values['field_options']['attach'] = '';
$field_values['field_options']['minnum'] = '0';
$field_values['field_options']['maxnum'] = '9999';
$field_values['field_options']['step'] = '1';
$field_values['field_options']['clock'] = '12';
$field_values['field_options']['start_time'] = '00:00';
$field_values['field_options']['end_time'] = '23:59';
$field_values['field_options']['unique'] = '0';
$field_values['field_options']['use_calc'] = '0';
$field_values['field_options']['calc'] = '';
$field_values['field_options']['duplication'] = '1';
$field_values['field_options']['rte'] = 'mce';
$field_values['field_options']['dyn_default_value'] = '';
$field_values['field_options']['multiple'] = '0';
$field_values['field_options']['unique_msg'] = 'This value must be unique.';
$field_values['field_options']['autocom'] = '0';
$field_values['field_options']['dependent_fields'] = '';
$field_values['field_options']['custom_field'] = 'course_promoprice';
$field_values['field_options']['post_field'] = 'post_custom';
$field_values['field_options']['taxonomy'] = 'category';
$field_values['field_options']['exclude_cat'] = '0';
$frm_field->create( $field_values );

    
$field_values = apply_filters('frm_before_field_created', FrmFieldsHelper::setup_new_vars('textarea', $form_id));
$field_values['id'] = 102;
$field_values['field_key'] = 'tc_course_promoinfo2';
$field_values['name'] = 'Course Promotion Information';
$field_values['description'] = '';
$field_values['type'] = 'textarea';
$field_values['default_value'] = '';
$field_values['options'] = '';
$field_values['required'] = '0';
$field_values['field_order'] = '16';
$field_values['field_options']['size'] = '';
$field_values['field_options']['max'] = '5';
$field_values['field_options']['label'] = '';
$field_values['field_options']['blank'] = 'This field cannot be blank.';
$field_values['field_options']['required_indicator'] = '*';
$field_values['field_options']['invalid'] = '';
$field_values['field_options']['separate_value'] = '0';
$field_values['field_options']['clear_on_focus'] = '0';
$field_values['field_options']['default_blank'] = '0';
$field_values['field_options']['classes'] = '';
$field_values['field_options']['custom_html'] = '<div id=\"frm_field_[id]_container\" class=\"frm_form_field form-field [required_class][error_class]\">
    <label class=\"frm_primary_label\">[field_name]
        <span class=\"frm_required\">[required_label]</span>
    </label>
    [input]
    [if description]<div class=\"frm_description\">[description]</div>[/if description]
    [if error]<div class=\"frm_error\">[error]</div>[/if error]
</div>';
$field_values['field_options']['slide'] = '0';
$field_values['field_options']['form_select'] = '';
$field_values['field_options']['show_hide'] = 'show';
$field_values['field_options']['any_all'] = 'any';
$field_values['field_options']['align'] = 'block';
$field_values['field_options']['hide_field'] = 'a:0:{}';
$field_values['field_options']['hide_field_cond'] = 'a:1:{i:0;s:2:"==";}';
$field_values['field_options']['hide_opt'] = 'a:0:{}';
$field_values['field_options']['star'] = '0';
$field_values['field_options']['ftypes'] = 'a:0:{}';
$field_values['field_options']['data_type'] = '';
$field_values['field_options']['restrict'] = '0';
$field_values['field_options']['start_year'] = '2000';
$field_values['field_options']['end_year'] = '2020';
$field_values['field_options']['read_only'] = '0';
$field_values['field_options']['admin_only'] = '';
$field_values['field_options']['locale'] = '';
$field_values['field_options']['attach'] = '';
$field_values['field_options']['minnum'] = '0';
$field_values['field_options']['maxnum'] = '9999';
$field_values['field_options']['step'] = '1';
$field_values['field_options']['clock'] = '12';
$field_values['field_options']['start_time'] = '00:00';
$field_values['field_options']['end_time'] = '23:59';
$field_values['field_options']['unique'] = '0';
$field_values['field_options']['use_calc'] = '0';
$field_values['field_options']['calc'] = '';
$field_values['field_options']['duplication'] = '1';
$field_values['field_options']['rte'] = 'mce';
$field_values['field_options']['dyn_default_value'] = '';
$field_values['field_options']['multiple'] = '0';
$field_values['field_options']['unique_msg'] = 'This value must be unique.';
$field_values['field_options']['autocom'] = '0';
$field_values['field_options']['dependent_fields'] = '';
$field_values['field_options']['custom_field'] = 'course_promoinfo';
$field_values['field_options']['post_field'] = 'post_custom';
$field_values['field_options']['taxonomy'] = 'category';
$field_values['field_options']['exclude_cat'] = '0';
$frm_field->create( $field_values );

