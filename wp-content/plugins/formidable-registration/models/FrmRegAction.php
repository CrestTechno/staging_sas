<?php

class FrmRegAction extends FrmFormAction {

	function __construct() {
		$action_ops = array(
		    'classes'   => 'frm_register_icon frm_icon_font',
            'limit'     => 1,
            'active'    => true,
            'force_event' => true,
            'priority'  => 30,
		);
		
	    $this->FrmFormAction('register', __('Register User', 'formidable'), $action_ops);
	}

	function form( $form_action, $args = array() ) {
	    extract($args);
	    
	    global $wpdb;
	    
	    $frm_field = new FrmField();
	    $fields = $frm_field->getAll($wpdb->prepare('fi.form_id=%d', $form->id) . " and fi.type not in ('end_divider', 'divider', 'html', 'break', 'captcha', 'rte')", ' ORDER BY field_order');
	    
	    include(FrmRegAppController::path() .'/views/_register_settings.php');
	}
	
	function get_defaults() {
	    return FrmRegAppHelper::get_default_options();
	}
}
