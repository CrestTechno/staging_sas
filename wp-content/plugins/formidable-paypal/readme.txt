== Changelog ==
2.03.01
- Make sure email is not sent when payment is complete if the box to hold the email is not checked
- Only send the delayed email if payment is successfully marked complete to prevent duplicate emails
- Removed version fallbacks and add minimum version requirement
- Use conditional logic rows from core plugin

2.03
- Added option to hold emails until after payment. This will also stop the registration emails.
- Added option to set a custom amount instead of requiring a field to be selected
- Added option for payments to be sent as donations
- Inserted additional logging for easier diagnosing where IPN is failing
- Allow values to be inserted into the item name field using the sidebar options, and removed the dropdown for inserting fields
- Extended conditional logic to more field types
- Switch options to new fields after form is duplicated

2.02
- Allow payments to be bulk deleted
- Only retrieve PayPal settings when they are used

2.01.01
- Remove more globals
- fix IPN notification

2.01
- Send info to PayPal after formatting the url values for languages
- Update the automatic update code
- Updates for strict standards
- Remove globals and defines

2.0.02
- Added column for form name in payment list 
- Added WPML integration to send to the language page in PayPal for the currently viewed language
- Make sure the user isn't sent to PayPal if nothing to pay

2.0.01
- Fix conditional logic for fields using separate values

2.0 
- Fix for redirecting without striping the | for processing IPN notification with Formidable 1.6.5 compatibility
- Get rid of warning message when checking for an updated version

2.0rc4 
- Update for 1.6.3 compatibility to save settings
- Update for conditional logic for fields with separate labels
- Update for Formidable v1.6.4+ to redirect to PayPal without an intermediate page
- Update logging information with added options of where to save the log

2.0rc3
- Added conditional logic for sending users to PayPal
- Fixed payment listing page for those who have customized the admin menu name
- Workaround for caching plugin conflict with saving settings

2.0rc2
- Added a message to show before redirection
- Added letters to the end of the invoice number to minimize conflicts
- Added option to add payments manually
- Moved settings for Formidable 1.6 compatibility
- Filter the item name/description for field shortcodes

2.0rc1
- Added functionality for deleting and editing payments
- Added frm_payment_paypal_ipn hook for actions after a payment is completed

== To Do ==
- recurring subscriptions
- quantity
