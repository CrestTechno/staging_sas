<div class="wrap">
    <div id="icon-edit-pages" class="icon32"><br/></div>
    <h2><?php _e('Edit Payment', 'formidable') ?>
        <a href="?page=formidable-payments&amp;action=new" class="add-new-h2"><?php _e('Add New', 'formidable'); ?></a>
    </h2>
    
    <div class="form-wrap">
        <?php 
        $path = method_exists('FrmAppHelper', 'plugin_path') ? FrmAppHelper::plugin_path() : FRM_PATH;
        include($path .'/classes/views/shared/errors.php');
        ?>

        <form method="post">
        <div id="poststuff" class="metabox-holder has-right-sidebar">
        <div class="inner-sidebar">
            <div id="submitdiv" class="postbox ">
            <h3 class="hndle"><span><?php _e('Publish', 'formidable') ?></span></h3>
            <div class="inside">
                <div class="submitbox">
                <div id="minor-publishing" style="border:none;">
                <div class="misc-pub-section">
                    <a href="?page=formidable-payments&amp;action=show&amp;frm_action=show&amp;id=<?php echo $payment['id']; ?>" class="button-secondary alignright"><?php _e('View', 'formidable') ?></a>
                    <div class="clear"></div>
                </div>
                </div>
                
                <div id="major-publishing-actions">
            	    <div id="delete-action">                	    
            	        <a class="submitdelete deletion" href="<?php echo add_query_arg('frm_action', 'destroy') ?>" onclick="return confirm('<?php _e('Are you sure you want to delete that payment?', 'formidable') ?>');" title="<?php _e('Delete', 'formidable') ?>"><?php _e('Delete', 'formidable') ?></a>
            	    </div>
            	    <div id="publishing-action">
                    <input type="submit" name="Submit" value="<?php _e('Update', 'formidable') ?>" class="button-primary" />
                    </div>
                    <div class="clear"></div>
                </div>
                </div>
            </div>
            </div>
        </div>
        
        <div id="post-body">
        <div id="post-body-content">
        <input type="hidden" name="id" value="<?php echo $payment['id'] ?>" />
        <?php 
        $form_action = 'update'; 
        wp_nonce_field('update-options'); 
        
        require(FrmPaymentsController::path() .'/views/payments/form.php'); 
        ?>

        <p>
        <input class="button-primary" type="submit" name="Submit" value="<?php _e('Update', 'formidable') ?>" /> 
        <?php _e('or', 'formidable') ?> 
        <a class="button-secondary cancel" href="?page=formidable-payments"><?php _e('Cancel', 'formidable') ?></a>
        </p>
        </div>
        </div>

        </form>
        </div>

        </div>
    </div>
    
</div>