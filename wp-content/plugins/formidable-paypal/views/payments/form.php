<input type="hidden" name="action" value="<?php echo $form_action ?>"/>

<table class="form-table"><tbody>
    <tr class="form-field">
        <th scope="row"><?php _e('Completed') ?></th>
        <td><input type="checkbox" value="1" name="completed" <?php checked($payment['completed'], 1) ?> /></td>
    </tr>
    
    <tr valign="top">
        <th scope="row"><?php _e('Entry') ?></th>
        <td>
            <select name="item_id">
                <option value=""></option>
            <?php foreach($entries as $entry){ ?>
                <option value="<?php echo $entry->id ?>" <?php selected($payment['item_id'], $entry->id) ?>><?php if(isset($users[$entry->user_id])) echo $users[$entry->user_id]; ?> #<?php echo $entry->id ?></option>
            <?php } ?>
            </select>
        </td>
    </tr>
    
    <tr valign="top">
        <th scope="row"><?php _e('Receipt') ?></th>
        <td><input type="text" name="receipt_id" value="<?php echo esc_attr($payment['receipt_id']) ?>" /></td>
    </tr>
    
    <tr valign="top">
        <th scope="row"><?php _e('Amount') ?></th>
        <td><?php echo $currency['symbol_left'] ?><input type="text" name="amount" value="<?php echo esc_attr($payment['amount']) ?>" /><?php echo $currency['symbol_right'] ?></td>
    </tr>
    
    <tr valign="top">
        <th scope="row"><?php _e('Date') ?></th>
        <td><input type="text" name="begin_date" class="" value="<?php echo $payment['begin_date'] ?>" /></td>
    </tr>
    
    <tr valign="top">
        <th scope="row"><?php _e('Payment Method') ?></th>
        <td><select name="paysys">
                <option value="paypal" <?php selected($payment['paysys'], 'paypal') ?>><?php _e('PayPal', 'formidable'); ?></option>
                <option value="manual" <?php selected($payment['paysys'], 'manual') ?>><?php _e('Manual', 'formidable'); ?></option>
            </select>
    </tr>
</tbody></table>