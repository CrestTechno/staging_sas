<?php
if ( method_exists('FrmProFormsController', 'include_logic_row') ) {
    _deprecated_file( basename(__FILE__), '1.07.05', null, __( 'This file no longer needs to be included.' ) );
    return;
}
?>

<div id="frm_logic_paypal_<?php echo $meta_name ?>" class="frm_logic_row_paypal">
<select name="options[paypal_list][hide_field][]" onchange="frmPayGetFieldValues(this.value,<?php echo $meta_name ?>)">
    <option value=""><?php _e('Select Field', 'formidable') ?></option>
    <?php foreach ($form_fields as $ff){ 
        if(!in_array($ff->type, array('select','radio','checkbox','10radio','scale')))
            continue;
        $selected = ($ff->id == $hide_field) ?' selected="selected"':''; ?>
    <option value="<?php echo $ff->id ?>"<?php echo $selected ?>><?php echo FrmAppHelper::truncate($ff->name, 30); ?></option>
    <?php } ?>
</select>
<?php _e('is', 'formidable'); 

if(!isset($values['paypal_list']['hide_field_cond']))
    $values['paypal_list']['hide_field_cond'] = array($meta_name => '==');

if(!isset($values['paypal_list']['hide_field_cond'][$meta_name]))
    $values['paypal_list']['hide_field_cond'][$meta_name] = '==';   
?>

<select name="options[paypal_list][hide_field_cond][]">
    <option value="==" <?php selected($values['paypal_list']['hide_field_cond'][$meta_name], '==') ?>><?php _e('equal to', 'formidable') ?></option>
    <option value="!=" <?php selected($values['paypal_list']['hide_field_cond'][$meta_name], '!=') ?>><?php _e('NOT equal to', 'formidable') ?> &nbsp;</option>
    <option value=">" <?php selected($values['paypal_list']['hide_field_cond'][$meta_name], '>') ?>><?php _e('greater than', 'formidable') ?></option>
    <option value="<" <?php selected($values['paypal_list']['hide_field_cond'][$meta_name], '<') ?>><?php _e('less than', 'formidable') ?></option>
    <option value="LIKE" <?php selected($values['paypal_list']['hide_field_cond'][$meta_name], 'LIKE') ?>><?php _e('like', 'formidable') ?></option>
    <option value="not LIKE" <?php selected($values['paypal_list']['hide_field_cond'][$meta_name], 'not LIKE') ?>><?php _e('not like', 'formidable') ?></option>
</select>

<span id="frm_pay_show_selected_values_<?php echo $meta_name ?>">
    <?php 
    if ($hide_field and is_numeric($hide_field)) {
        $frm_field = new FrmField();
        $new_field = $frm_field->getOne($hide_field);
        unset($frm_field);
    }

    include('_field_values.php');
    ?>
</span>

<a class="frm_remove_tag frm_icon_font" data-removeid="frm_logic_paypal_<?php echo $meta_name ?>" data-showlast=".frm_add_paypal_logic"></a>
<a class="frm_add_tag frm_icon_font" href="javascript:frmPayAddLogicRow();"></a>

</div>