<?php

class FrmPaymentsListHelper extends WP_List_Table {

	function FrmPaymentsListHelper() {
	    parent::__construct( array(
			'plural' => 'payments',
			'singular' => 'payment',
		) );
	}

	function ajax_user_can() {
		return current_user_can( 'administrator' );
	}

    function prepare_items() {
        global $wpdb;
        
    	$orderby = ( isset( $_REQUEST['orderby'] ) ) ? $_REQUEST['orderby'] : 'created_at';
		$order = ( isset( $_REQUEST['order'] ) ) ? $_REQUEST['order'] : 'DESC';

		$s = isset( $_REQUEST['s'] ) ? $_REQUEST['s'] : '';
    	$page = $this->get_pagenum();
        $per_page = $this->get_items_per_page( 'formidable_page_formidable_payments_per_page');
		$start = ( isset( $_REQUEST['start'] ) ) ? $_REQUEST['start'] : (( $page - 1 ) * $per_page);
    	
    	$frmdb = new FrmDb();
    	$this->items = $frmdb->get_records($wpdb->prefix .'frm_payments', array(), trim("$orderby $order"), "$start, $per_page");
    	$total_items = $frmdb->get_count($wpdb->prefix .'frm_payments', array());
    	
    	$this->set_pagination_args( array(
			'total_items' => $total_items,
			'per_page' => $per_page
		) );
    }

    function no_items() {
    	_e( 'No payments found.' );
    }

	function get_columns() {
	    return FrmPaymentsController::payment_columns();
	}
	

	function get_sortable_columns() {
		return array(
		    'item_id'   => 'item_id',
			'completed'  => 'completed',
			'amount'     => 'amount',
			'created_at' => 'created_at',
			'receipt_id' => 'receipt_id',
			'begin_date' => 'begin_date',
			//'expire_date' => 'expire_date',
			'paysys'     => 'paysys'
		);
	}
	
	function get_bulk_actions(){
	    $actions = array('bulk_delete' => __('Delete'));
            
        return $actions;
    }

    function display_rows() {
        global $wpdb, $frmpro_settings;
        
    	$alt = 0;
        $base_link = '?page=formidable-payments&action=';
        
        $entry_ids = array();
        foreach ( $this->items as $item ) {
            $entry_ids[] = $item->item_id;
            unset($item);
        }
        
        $forms = $wpdb->get_results("SELECT fo.id as form_id, fo.name, e.id FROM {$wpdb->prefix}frm_items e LEFT JOIN {$wpdb->prefix}frm_forms fo ON (e.form_id = fo.id) WHERE e.id in (". implode(',', $entry_ids ).")");
        unset($entry_ids);
        
        $form_ids = array();
        foreach($forms as $form){
            $form_ids[$form->id] = $form;
            unset($form);
        }
        
		foreach ( $this->items as $item ) {
            $item->completed = ( $item->completed ) ? __( 'Yes' ) : __( 'No' );
            $item->amount = FrmPaymentsHelper::formatted_amount( $item->amount );
			$style = ( $alt++ % 2 ) ? '' : ' class="alternate"';

			$edit_link = $base_link .'edit&id='.$item->id;
			$view_link = $base_link .'show&id='.$item->id;
			$delete_link = $base_link .'destroy&id='.$item->id;
?>
    	    <tr id="payment-<?php echo $item->id; ?>" valign="middle" <?php echo $style; ?>>
<?php

    		list( $columns, $hidden ) = $this->get_column_info();

    		foreach ( $columns as $column_name => $column_display_name ) {
    			$class = "class='column-$column_name'";

    			$style = '';
    			if ( in_array( $column_name, $hidden ) )
    				$style = ' style="display:none;"';

    			$attributes = $class . $style;

    			switch ( $column_name ) {
    				case 'cb':
    					echo '<th scope="row" class="check-column"><input type="checkbox" name="item-action[]" value="'. esc_attr( $item->id ) .'" /></th>';
    				    break;

    				case 'receipt_id':
    					echo "<td $attributes><strong><a class='row-title' href='$edit_link' title='" . __( 'Edit' ) . "'>$item->receipt_id</a></strong><br />";

    					$actions = array();
    					$actions['view'] = '<a href="' . $view_link . '">' . __( 'View' ) . '</a>';
    					$actions['edit'] = '<a href="' . $edit_link . '">' . __( 'Edit' ) . '</a>';
    					$actions['delete'] = '<a href="' . $delete_link . '">' . __( 'Delete' ) . '</a>';
    					echo $this->row_actions( $actions );

    					echo '</td>';
    					break;
    				case 'user_id':
    				    $entry_user = $wpdb->get_var($wpdb->prepare("SELECT user_id FROM {$wpdb->prefix}frm_items WHERE id=%d", $item->item_id));
    				    
    				    if(class_exists('FrmProFieldsHelper'))
                            echo "<td $attributes>". FrmProFieldsHelper::get_display_name($entry_user, 'display_name', array('link' => true)) ."</td>";
                        else
                            echo "<td $attributes>$entry_user</td>";
                            
                        break;
    				case 'item_id':
    				    echo "<td $attributes><a href='?page=formidable-entries&frm_action=show&action=show&id={$item->item_id}'>{$item->item_id}</a></td>";
    					break;
    				case 'form_id':
    				    echo "<td $attributes>". (isset($form_ids[$item->item_id]) ? $form_ids[$item->item_id]->name : '') ."</td>";
    				    break;
    				case 'created_at':
    				case 'begin_date':
    				case 'expire_date':
    				    if(strtotime($item->{$column_name}) < 1){
    				        $item->{$column_name} = '';
    				    }else if(class_exists('FrmProAppHelper')){
    				        $date = date($frmpro_settings->date_format, strtotime($item->{$column_name}));
        					$item->{$column_name} = "<abbr title='". date($frmpro_settings->date_format .' g:i:s A', strtotime($item->{$column_name})) ."'>". $date ."</abbr>";
    				    }
    				    
    				    ?>
					    <td <?php echo $attributes ?>><?php echo $item->{$column_name} ?></td>
					    <?php
    				    break;
    				default:
    					?>
    					<td <?php echo $attributes ?>><?php echo $item->{$column_name} ?></td>
    					<?php
    					break;
    			}
    		}
    ?>
    		</tr>
    <?php
        unset($item);
    	}
    }
}
