<?php
class FrmPaymentSettingsController{
    public static function load_hooks(){
        add_action('frm_add_settings_section', 'FrmPaymentSettingsController::add_settings_section');
        add_action('frm_add_form_settings_section', 'FrmPaymentSettingsController::add_payment_options');
        add_action('wp_ajax_frm_pay_add_logic_row', 'FrmPaymentSettingsController::add_logic_row');
        add_action('wp_ajax_frm_pay_get_field_values', 'FrmPaymentSettingsController::get_field_values');
        add_action('frm_after_duplicate_form', 'FrmPaymentSettingsController::duplicate', 15, 2);
    }
    
    public static function add_settings_section($sections){
        $sections['payment'] = array('class' => 'FrmPaymentSettingsController', 'function' => 'route');
        return $sections;
    }
    
    public static function add_payment_options($sections){
        $sections['payment'] = array('class' => 'FrmPaymentSettingsController', 'function' => 'payment_options');
        return $sections;
    }
    
    public static function payment_options($values){
        if(isset($values['id'])){
            $frm_field = new FrmField();
            $form_fields = $frm_field->getAll("fi.form_id='$values[id]' and fi.type not in ('divider', 'html', 'break', 'captcha', 'rte', 'form')", ' ORDER BY field_order');
            unset($frm_field);
        }
        $hide_paypal = ($values['paypal']) ? '' : 'style="display:none;"';
        
        $show_amount = ($values['paypal_amount'] == '') ? false : true;
        
        include(FrmPaymentsController::path() .'/views/settings/payment_options.php');
    }
    
    public static function add_logic_row(){
        $form_id = (int)$_POST['form_id'];
        $meta_name = $_POST['meta_name'];
        $hide_field = '';
        
        if ( !empty($_POST['ext']) ) {
            $paypal_list = array('hide_field' => array(), 'hide_field_cond' => array($meta_name => '=='), 'hide_opt' => array());
            self::include_logic_row($meta_name, $form_id, $paypal_list);
        } else {
            $frm_field = new FrmField();
            $form_fields = $frm_field->getAll(array('fi.form_id' => (int) $form_id), 'field_order');
            unset($frm_field);

            global $wpdb;
            $values = $wpdb->get_var($wpdb->prepare("SELECT options FROM {$wpdb->prefix}frm_forms WHERE id=%d", $form_id));
            $values = maybe_unserialize($values);
            if(!isset($values['paypal_list']) or empty($values['paypal_list']))
                $values['paypal_list'] = array('hide_field' => array(), 'hide_field_cond' => array(), 'hide_opt' => array());

            if(!isset($values['paypal_list']['hide_field_cond'][$meta_name]))
                $values['paypal_list']['hide_field_cond'][$meta_name] = '==';
            
            include(FrmPaymentsController::path() .'/views/settings/_logic_row.php');
        }
        
        die();
    }
    
    public static function include_logic_row($meta_name, $form_id, $values) {
        FrmProFormsController::include_logic_row(array(
            'meta_name' => $meta_name,
            'condition' => array(
                'hide_field'    => ( isset($values['hide_field']) && isset($values['hide_field'][$meta_name]) ) ? $values['hide_field'][$meta_name] : '',
                'hide_field_cond' => ( isset($values['hide_field_cond']) && isset($values['hide_field_cond'][$meta_name]) ) ? $values['hide_field_cond'][$meta_name] : '',
                'hide_opt'      => ( isset($values['hide_opt']) && isset($values['hide_opt'][$meta_name]) ) ? $values['hide_opt'][$meta_name] : '',
            ),
            'type' => 'paypal',
            'showlast' => '.frm_add_paypal_logic',
            'key' => 'paypal',
            'form_id' => $form_id,
            'id' => 'frm_logic_paypal_'. $meta_name,
            'names' => array(
                'hide_field'    => 'options[paypal_list][hide_field][]',
                'hide_field_cond' => 'options[paypal_list][hide_field_cond][]',
                'hide_opt'      => 'options[paypal_list][hide_opt][]',
            ),
        ));
    }
    
    public static function get_field_values(){
        $form_id = (int)$_POST['form_id'];
        $meta_name = $_POST['meta_name'];
        
        $frm_field = new FrmField();
        $new_field = $frm_field->getOne($_POST['field_id']);
        unset($frm_field);
        
        global $wpdb;
        $values = $wpdb->get_var($wpdb->prepare("SELECT options FROM {$wpdb->prefix}frm_forms WHERE id=%d", $form_id));
        $values = maybe_unserialize($values);
        if(!isset($values['paypal_list']) or empty($values['paypal_list']))
            $values['paypal_list'] = array('hide_field' => array(), 'hide_field_cond' => array(), 'hide_opt' => array());
            
        require(FrmPaymentsController::path() .'/views/settings/_field_values.php');
        die();
    }
        
    public static function display_form($errors=array(), $message=''){
        $frm_payment_settings = new FrmPaymentSettings();
        
        require(FrmPaymentsController::path() .'/views/settings/form.php');
    }

    public static function process_form(){
        $frm_payment_settings = new FrmPaymentSettings();

        //$errors = $frm_payment_settings->validate($_POST,array());
        $errors = array();
        $frm_payment_settings->update($_POST);

        if( empty($errors) ){
            $frm_payment_settings->store();
            $message = __('Settings Saved', 'formidable');
        }
        
        self::display_form($errors, $message);
    }

    public static function route(){
        $action = isset($_REQUEST['frm_action']) ? 'frm_action' : 'action';
        $action = FrmAppHelper::get_param($action);
        if($action == 'process-form')
            return self::process_form();
        else
            return self::display_form();
    }
    
    // switch field keys/ids after form is duplicated
    public static function duplicate($id, $values) {
        if ( method_exists( 'FrmProFieldsHelper', 'switch_field_ids' ) ) {
            // don't switch IDs unless running Formidabe version that does
            return;
        }
        
        $frm_form = new FrmForm();
        $form = $frm_form->getOne($id);
        unset($frm_form);
        
        $new_opts = $values['options'] = $form->options;
        unset($form);
        
        if ( !isset($values['options']['paypal_item_name']) || empty($values['options']['paypal_item_name']) ) {
            // don't continue if there aren't paypal settings to switch
            return;
        }
        
        global $frm_duplicate_ids;
        
        if ( is_numeric($new_opts['paypal_amount_field']) && isset($frm_duplicate_ids[$new_opts['paypal_amount_field']]) ) {
            $new_opts['paypal_amount_field'] = $frm_duplicate_ids[$new_opts['paypal_amount_field']];
        }
        
        $new_opts['paypal_item_name'] = FrmProFieldsHelper::switch_field_ids($new_opts['paypal_item_name']);
        
        // switch conditional logic
        if ( is_array($new_opts['paypal_list']) && isset($new_opts['paypal_list']['hide_field']) ) {
            foreach ( (array) $new_opts['paypal_list']['hide_field'] as $ck => $cv ) {
                if ( is_numeric($cv) && isset($frm_duplicate_ids[$cv]) ) {
                    $new_opts['paypal_list']['hide_field'][$ck] = $frm_duplicate_ids[$cv];
                }
                
                unset($ck);
                unset($cv);
            }
        }
        
        if ( $new_opts != $values['options'] ) {
            global $wpdb;
            $wpdb->update($wpdb->prefix .'frm_forms', array('options' => maybe_serialize($new_opts)), array('id' => $id));
        }
    }

}