<?php
/*
Plugin Name: Formidable Payment
Description: Send Posted results to PayPal
Version: 2.03.01
Plugin URI: http://formidablepro.com/
Author URI: http://strategy11.com
Author: Strategy11
*/

//Controllers
require(dirname( __FILE__ ) .'/controllers/FrmPaymentsController.php');
require(dirname( __FILE__ ) .'/controllers/FrmPaymentSettingsController.php');

FrmPaymentsController::load_hooks();
FrmPaymentSettingsController::load_hooks();

// SETUP SETTINGS OBJECT
require(dirname( __FILE__ ) .'/models/FrmPaymentSettings.php');

require(dirname( __FILE__ ) .'/helpers/FrmPaymentsHelper.php');
