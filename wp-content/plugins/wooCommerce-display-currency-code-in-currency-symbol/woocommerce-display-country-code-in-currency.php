<?php
/**
* Plugin Name: WooCommerce Display Currency Code in Currency Symbol
* Plugin URI: https://gist.github.com/BFTrick/10681832
* Description: Add the currency code to the currency symbol in WooCommerce. Ex. USD $.
* Author: Patrick Rauland
* Author URI: http://patrickrauland.com/
* Version: 1.0
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
*/
function patricks_currency_symbol( $currency_symbol, $currency ) {
switch( $currency ) {
case 'USD':
$currency_symbol = 'USD $';
break;
case 'NZD':
$currency_symbol = 'NZD $';
break;
case 'AUD':
$currency_symbol = 'AU$';
break;
}
return $currency_symbol;
}
add_filter('woocommerce_currency_symbol', 'patricks_currency_symbol', 30, 2); 