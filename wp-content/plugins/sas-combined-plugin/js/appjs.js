jQuery(document).ready(function()
 {
jQuery('.apptooltip').each(function() { 
    jQuery(this).qtip({
        content: {
            text: jQuery(this).next('div') 
        },
		position: {
        my: 'top left',  
        at: 'top left', 
    	},
		style: { 
		classes: 'qtip-light qtip-shadow',
		width: '210px' 
		},
		show: {
             solo: true
         },
        hide: {
             fixed: true,
             delay: 300
        }
    });
});
 });
 
 
 