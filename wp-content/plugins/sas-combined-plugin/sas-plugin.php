<?php
/*
Plugin Name: S-A-S Website Functionality Plugin
Plugin URI: http://www.s-a-s.com.au
Description: Functionality plugin for Training Courses, Products, Member Functions and Payments for www.s-a-s.com.au
Version: 1.0
Author: ETS.io
Author URI: http://ets.io
*/

//require WP_PLUGIN_DIR . '/options-framework-plugin/options-framework.php';
require WP_PLUGIN_DIR . '/training-courses/wp-post-formats/cf-post-formats.php';

function plugin_scripts()  
    {  
        wp_register_script( 'app-script', plugins_url( '/js/appjs.js', __FILE__ ) );  
		wp_register_script( 'app-qtip', plugins_url( '/js/jquery.qtip.min.js', __FILE__ ) );  
		wp_register_script( 'app-slides', plugins_url( '/js/jquery.slides.min.js', __FILE__ ) ); 
		//wp_register_script( 'app-imagesloaded', plugins_url( '/js/imagesloaded.min.js', __FILE__ ) );
 		wp_register_style( 'app-style', plugins_url( '/css/appstyle.css', __FILE__ ), array(), '20120208', 'all' ); 
		wp_register_style( 'app-qtipstyle', plugins_url( '/css/jquery.qtip.min.css', __FILE__ ), array(), '20120208', 'all' ); 

        wp_enqueue_script( 'app-script' ); 
		wp_enqueue_script( 'app-qtip' );
		wp_enqueue_script( 'app-slides' );
		//wp_enqueue_script( 'app-imagesloaded' );
		wp_enqueue_style( 'app-style' ); 
		wp_enqueue_style( 'app-qtipstyle' ); 
    }  
add_action( 'wp_enqueue_scripts', 'plugin_scripts' );  


// Load CMB
//function load_cmb() {
 // if (!is_admin()) {
 //  return;
 //}

 // require WP_PLUGIN_DIR . '/training-courses/cmb/init.php';
//}
//add_action('init', 'load_cmb');

//require_once(dirname(__FILE__) . '/lib/post-types.php');
//require_once(dirname(__FILE__) . '/lib/meta-boxes.php');
// require_once(dirname(__FILE__) . '/lib/shortcodes.php');

function get_images_from_media_library() {
global $post;
$query_images = get_children( array( 'post_parent' => get_the_ID(), 'post_type' => 'attachment', 'post_mime_type' => 'image', 'orderby' => 'menu_order', 'order' => 'ASC', 'numberposts' => 999 ) ); 

    $query_images = get_children( $args );
    $images = array();
    foreach ( $query_images as $image) {
        $images[]= $image->guid;
    }
    return $images;
}

function display_images_from_media_library() {

	$imgs = get_images_from_media_library();
	$html = '<div id="slides">';
	
	foreach($imgs as $img) {
	
		$html .= '<img src="' . $img . '" alt="" />';
	
	}
	
	$html .= '</div>';
	
	return $html;

}

function show_all_children( $post_id, $current_level ) {
    $children = get_posts( array(
        'post_type' =>'base_apps',
        'posts_per_page' =>-1,
        'post_parent' => $post_id,
        'order_by' => 'title',
        'order' => 'ASC' ) );
    if ( empty($children) ) return;

    echo '<ul class="children level-'.$current_level.'-children">';

    foreach ($children as $child) {

        /* Here would be the point where you
            do whatever you want to display the 
            posts. The variable $current_level can
            be used if you want to style different 
            levels in the hierarchy differently */

            echo '<li>';

        echo '<a href="'.get_permalink($child->ID). $child->ID . '">';
        echo apply_filters( 'the_title', $child->post_title );
        echo '</a>';

        // now call the same function for child of this child
        //show_all_children( $child->ID, $current_level+1 );

            echo '</li>';

        }

    echo '</ul>';
wp_reset_query();
    }
	
function show_all_parents( $post_id, $current_level ) {
    $children = get_posts( array(
        'post_type' =>'base_apps',
        'posts_per_page' =>1,
        'post_child' => $post_id,
        'order_by' => 'title',
        'order' => 'ASC' ) );
    if ( empty($children) ) return;

    echo '<ul class="children level-'.$current_level.'-children">';

                    echo '<li>';

        echo '<a href="'.get_permalink($children[0]->ID).'">';
        echo apply_filters( 'the_title', $children[0]->post_title );
        echo '</a>';

        // now call the same function for child of this child
        //show_all_parents( $children[0]->ID, $current_level+1 );

            echo '</li>';


    echo '</ul>';
wp_reset_query();
    }
/*	
add_filter('frm_get_default_value', 'my_custom_default_value', 10, 2);
function my_custom_default_value($new_value, $field){
global $post;
  if($field->id == 298){ //change 25 to the ID of the field
    if ( is_singular( 'training_courses' ) ) {
    	$courseprice = get_post_meta( $post->ID, 'course_price', true );
		$coursediscount = get_post_meta( $post->ID, 'course_promoprice', true );
		   if ($coursediscount != '') {
			   $new_value = $coursediscount;
			} else {
				$new_value = $courseprice;
			}
	}
	if ( is_singular( 'products' ) ) {
    	$productprice = get_post_meta( $post->ID, 'product_price', true );
		$productdiscount = get_post_meta( $post->ID, 'product_discount', true );
			if ($productdiscount != '') {
			   $new_value = $productdiscount;
			} else {
				$new_value = $productprice;
			}
	}
	
  }
  return $new_value;
}
*/
//Update Mime types available
//add_filter('upload_mimes','restrict_mime');
//function restrict_mime($mimes) {
//    $mimes['mp4'] = 'video/mp4';
 //   $mimes['csv'] = 'text/csv'; //allow csv files
 //   $mimes['avi'] = 'video/x-msvideo'; //add avi
 //   $mimes['mov'] = 'video/quicktime'; //add mov
//	$mimes['doc'] = 'application/msword';  // add word 97
//	$mimes['docx'] = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';  // add word 2000+
//	$mimes['xls'] = 'application/x-msexcel';  // add excel 97
//	$mimes['xlsx'] = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';  // add word 2000+
//	$mimes['xlsm'] = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';  // add word 2000+
//	$mimes['ppt'] = 'application/x-mspowerpoint';  // add powerpoint 97
//	$mimes['pptx'] = 'application/vnd.openxmlformats-officedocument.presentationml.presentation';  // add powerpoint 2000+
//	$mimes['zip'] = 'application/x-compressed'; //add avi
//	
//	
 //   return $mimes;
//}

//add_action('frm_after_create_entry', 'after_entry_created', 41, 2);
//function after_entry_created($entry_id, $form_id){
 // if($form_id == 7){ //change 5 to the ID of your form
 //   global $frm_entry;
  //  $entry = $frm_entry->getOne($entry_id);
  //  update_post_meta($entry->post_id, 'vendor_entryid', $entry_id); //change frm_entry_id to the name of your custom field
 // }
//}

//add_action('frm_after_create_entry', 'after_entry_created_two', 41, 2);
//function after_entry_created_two($entry_id, $form_id){
//  if($form_id == 19){ //change 5 to the ID of your form
 //   global $frm_entry;
//    $entry = $frm_entry->getOne($entry_id);
 //   update_post_meta($entry->post_id, 'vendor_entryid', $entry_id); //change frm_entry_id to the name of your custom field
//  }
//}


// Delay Product Download Email until Payment Received
//add_filter('frm_to_email', 'stop_the_email', 20, 3 );
//function stop_the_email($emails, $values, $form_id){
//if($form_id == 20){ //change 5 to the ID of your form
//   if(isset($_POST) and ((isset($_POST['payment_completed']) and $_POST['payment_completed']) or (isset($_POST['action']) and $_POST['action'] == 'send_email')))
//      return $emails;
//   else
 //     $emails = array();
//}
//return $emails;
//}

//Trigger the email to send after a payment is completed:
//add_action('frm_payment_paypal_ipn', 'send_email_now');
//function send_email_now($vars){
 //  if(!$vars['pay_vars']['completed']) //only send the email if payment is completed
 //     return;
//   global $frmdb, $frmpro_notification;
 //  $entry_id = $vars['payment']->item_id;
//  $form_id = $frmdb->get_var($frmdb->entries, array('id' => $entry_id), 'form_id');
 //  $_POST['payment_completed'] = true; //to let the other function know to send the email
 //  $frmpro_notification->entry_created($entry_id, $form_id);
//}

//Update payment field
//add_action('frm_payment_paypal_ipn', 'mark_as_complete');
//function mark_as_complete($vars){
 //  if(!$vars['pay_vars']['completed'])
  //    return; //don't change value if the payment was not completed
 //  global $frmdb, $wpdb;
 //  $wpdb->update($frmdb->entry_metas, array('meta_value' => 'Paid'), array('item_id' => $vars['payment']->item_id, 'field_id' => 443)); //change 25 to the ID of the completed field
//}

// Update Order Type on order
/*
add_filter('frm_after_create_entry', 'update_order_type_field', 30, 2);
function update_order_type_field($entry_id, $form_id){
 if($form_id == 20){ //replace 5 with the id of the form
    global $frmdb;
	$theposttype = get_post_type( get_the_ID());
	if ($theposttype == 'training_courses') { $theposttype = 'course'; }
	if ($theposttype == 'products') { $theposttype = 'product'; }
    $order_type = $_POST['item_meta'][485]; //change 25 to the ID of your data from entries field
    FrmProEntriesController::update_field_ajax($entry_id, 485, $theposttype); //this updates the date field in the parent form. Change 26 the the ID of the date field in the parent form
}
}
*/
/*
// Update Order Type on order
add_filter('frm_validate_field_entry', 'update_order_type', 8, 3);
function update_order_type($errors, $posted_field, $posted_value){
  if($posted_field->id == 485){ //change 25 to the ID of the hidden field (in two places)
  $theposttype = get_post_type( get_the_ID());
  if ($theposttype == 'training_courses') { $theposttype = 'course'; }
  if ($theposttype == 'products') { $theposttype = 'product'; }
    $_POST['item_meta'][485] = $theposttype;
    //change 20 and 21 to the IDs of your first and last name fields
  }
  return $errors;
}
*/
// Update Order Type on order
//add_filter('frm_get_default_value', 'post_type_default_value', 10, 2);
//function post_type_default_value($new_value, $field){
//  if($field->id == 485){ 
 //  $theposttype = get_post_type( get_the_ID());
 //    if ($theposttype == 'training_courses') { $new_value = 'course'; }
 //    if ($theposttype == 'products') { $new_value = 'product'; }  
 // }
 // return $new_value;
//}

// Update Order Type on order
//add_filter('frm_get_default_value', 'status_default_value', 10, 2);
//function status_default_value($new_value, $field){
//  if($field->id == 443){ 
  // $theposttype = get_post_type( get_the_ID());
   //  if ($theposttype == 'training_courses') { $new_value = 'Training Pending'; }
   //  if ($theposttype == 'products') { $new_value = 'Product Pending'; }  
  //}
 // return $new_value;
//}


//stop from sending to PayPal on create product 
//add_action('frm_after_create_entry', 'stop_frm_to_paypal', 40, 2);
//function stop_frm_to_paypal($entry_id, $form_id){
 // if($form_id == 20 and isset($_POST['frm_payment'])){ //change 5 to the ID of your form
  // $ordertype = $_POST['item_meta'][485];
   //$status = $_POST['item_meta'][443];
   // if ($ordertype == 'course') {
   //   unset($_POST['frm_payment']);
   //} else {}
  //}
//}


//Update Payment Form Statuses
//add_action('frm_after_update_entry', 'after_entry_status_updated', 50, 2);
//function after_entry_status_updated($entry_id, $form_id){
 // if($form_id == 20) {
  // $ordertype = $_POST['item_meta'][485];
  // $status = $_POST['item_meta'][443];
 //  $clientconfirm = $_POST['item_meta'][639];
  //  if ($status == 'Training Confirmed' && $clientconfirm == 'Yes') {  
 //   	FrmPaymentsController::redirect_for_payment($entry_id, $form_id);
//	}
 // }
//}

//Change FacetWP Selection Box HTML
function my_facetwp_selections_html( $output, $params ) {
    $output = '';
    $selections = $params['selections'];
    if ( !empty( $selections ) ) {
        $output .= '<h4>Your Selections</h4><ul>';
        foreach ( $selections as $facet_name => $selection_type ) {
            foreach ( $selection_type as $key => $selection ) {
                $output .= '<li data-facet="' . $facet_name . '" data-value="' . $key . '">';
                $output .= '<h3><span class="label label-primary">' . $selection . '</span></h3>';
                $output .= '</li>';
            }
        }
        $output .= '</ul>';
    }
    return $output;
}
add_filter( 'facetwp_selections_html', 'my_facetwp_selections_html', 10, 2 );



