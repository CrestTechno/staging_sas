<?php while (have_posts()) : the_post(); ?>
<?php $vendorid = $post->post_author;
$all_meta_for_user = get_user_meta( $vendorid );
$logo_attr = array(
	'src'	=> $src,
	'class'	=> "aligncenter attachment-$size",
	'alt'   => trim(strip_tags( get_post_meta($attachment_id, '_wp_attachment_image_alt', true) )),
); ?>
  <article <?php post_class(); ?>>
    <header>
      <h1 class="entry-title"><?php the_title(); ?></h1>
      <?php // get_template_part('templates/entry-meta'); ?>
    </header>
    <div class="entry-content">

<div class="row">
  <div class="col-md-8">
  <h4>Course Overview</h4>
  <p><?php echo get_post_meta( $post->ID, 'course_overview', true ); ?></p>
  <h4>Course Information</h4>
  <p><?php echo get_post_meta( $post->ID, 'course_information', true ); ?></p>
  <?php global $wp_embed;?>
  <?php if (get_post_meta( $post->ID, 'course_video', true ) != '') { ?>
  <div class="videowrapper"><?php echo $wp_embed->run_shortcode('[embed]' . get_post_meta( $post->ID, 'course_video', true ) . '&wmode=opaque[/embed]'); ?></div>
  <?php } ?>
  </div>
  <div class="col-md-4">
    <ul class="list-group">
    <li class="list-group-item">Course Provided by: <br /><?php echo wp_get_attachment_image( $all_meta_for_user['Business - Logo'][0], array(150,150), $logo_attr  ); ?><br /><h4><?php echo $all_meta_for_user['Company Name'][0]; ?></h4><p><?php echo $all_meta_for_user['Business Address'][0]; ?>, <?php echo $all_meta_for_user['Business City'][0]; ?>, <?php echo $all_meta_for_user['Business State'][0]; ?></p><p>Telephone: <?php echo $all_meta_for_user['Business Phone'][0]; ?></p><p><a href="<?php echo $all_meta_for_user['Business Website'][0]; ?>">Website</a></p></li>
    <li class="list-group-item">Course Code: <strong><?php echo get_post_meta( $post->ID, 'course_code', true ); ?></strong></li>
    <li class="list-group-item">Location Address: <strong><?php echo get_post_meta( $post->ID, 'course_location', true ); ?></strong></li>
    <li class="list-group-item">Duration: <strong><?php echo get_post_meta( $post->ID, 'course_duration', true ); ?></strong></li>
    <li class="list-group-item">Start: <strong><?php echo get_post_meta( $post->ID, 'course_startdate', true ); ?> ( <?php echo get_post_meta( $post->ID, 'course_starttime', true ); ?> )</strong></li>
    <li class="list-group-item">End: <strong><?php echo get_post_meta( $post->ID, 'course_enddate', true ); ?> ( <?php echo get_post_meta( $post->ID, 'course_endtime', true ); ?> )</strong></li>
    <li class="list-group-item">Accreditations: <strong><?php echo get_post_meta( $post->ID, 'course_accreditations', true ); ?></strong></li>
    <li class="list-group-item">Requirements: <strong><?php echo get_post_meta( $post->ID, 'course_requirements', true ); ?></strong></li>
    <li class="list-group-item">Price: <strong>AU$ <?php echo get_post_meta( $post->ID, 'course_price', true ); ?></strong></li>
    <li class="list-group-item"><a type="button" href="#ordernow" class="btn btn-primary btn-lg btn-block">Book Now</a></li>
    <li class="list-group-item"><h4>Location Map</h4><?php $courselocation = get_post_meta( $post->ID, 'course_location', true ); echo do_shortcode('[google-map-v3 shortcodeid="TO_BE_GENERATED" width="100%" height="350" zoom="12" maptype="roadmap" mapalign="center" directionhint="false" language="default" poweredby="false" maptypecontrol="true" pancontrol="true" zoomcontrol="true" scalecontrol="true" streetviewcontrol="true" scrollwheelcontrol="false" draggable="true" tiltfourtyfive="false" enablegeolocationmarker="false" addmarkermashup="false" addmarkermashupbubble="false" addmarkerlist="' .$courselocation . '{}1-default.png{}Course" bubbleautopan="true" distanceunits="miles" showbike="false" showtraffic="false" showpanoramio="false"]'); ?></li>
    </ul>
  </div>
</div>

<div id="ordernow">
  <h4>Make an Enquiry</h4>
  <?php echo FrmAppController::get_form_shortcode(array('id' => 20, 'key' => '', 'title' => false, 'description' => false, 'exclude_fields' => '443,485')); ?>
</div>
    
      <?php the_content(); ?>
    </div>
    <footer>
      <?php wp_link_pages(array('before' => '<nav class="page-nav"><p>' . __('Pages:', 'ets'), 'after' => '</p></nav>')); ?>
    </footer>
    <?php comments_template('/templates/comments.php'); ?>
  </article>
<?php endwhile; ?>
