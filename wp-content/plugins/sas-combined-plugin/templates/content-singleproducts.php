<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>
    <header>
      <h1 class="entry-title"><?php the_title(); ?></h1>
      <?php // get_template_part('templates/entry-meta'); ?>
    </header>
    <div class="entry-content">
    
<div class="row">
  <div class="col-md-4"><img class="img-responsive" src="<?php echo wp_get_attachment_url( get_post_meta( $post->ID, 'product_image', true ) ); ?>" /></div>
  <div class="col-md-4"><?php echo get_post_meta( $post->ID, 'product_shortdesc', true ); ?></div>
  <div class="col-md-4">
    <ul class="list-group">
    <li class="list-group-item">Product Code: <strong><?php echo get_post_meta( $post->ID, 'product_code', true ); ?></strong></li>
    <li class="list-group-item">Price: <?php if (get_post_meta( $post->ID, 'product_discount', true ) == '0') { echo 'AU$ ' . get_post_meta( $post->ID, 'product_price', true ); } elseif (get_post_meta( $post->ID, 'product_discount', true ) > '0') { $price = get_post_meta( $post->ID, 'product_price', true ); $discount = get_post_meta( $post->ID, 'product_discount', true ); $discountprice = $price * ((100-$discount) / 100); echo '<span style="text-decoration: line-through;"> AU$ ' . get_post_meta( $post->ID, 'product_price', true) . '</span><strong> AU$ '; echo $discountprice;  } ?></strong></li>
    <li class="list-group-item"><a type="button" href="#ordernow" class="btn btn-primary btn-lg btn-block">Order Now</a></li>
    <?php if (get_post_meta( $post->ID, 'product_sample', true ) != '') { ?><li class="list-group-item"><a type="button" href="<?php echo wp_get_attachment_url( get_post_meta( $post->ID, 'product_sample', true ) ); ?>" class="btn btn-default btn btn-block">Download Sample</a></li><? } ?>
    </ul>
  </div>
</div>  

<div class="row">
  <div class="col-md-12"><h3>Product Description</h3><p><?php echo get_post_meta( $post->ID, 'product_fulldesc', true ); ?></p></div>
  <?php global $wp_embed;?>
  <?php if (get_post_meta( $post->ID, 'product_video', true ) != '') { ?>
  <div class="col-md-12"><div class="videowrapper"><?php echo $wp_embed->run_shortcode('[embed]' . get_post_meta( $post->ID, 'product_video', true ) . '&wmode=opaque[/embed]'); ?></div></div>
  <?php } ?>
</div>

<div id="ordernow">
  <h4>Place Your Order</h4>
  <?php echo FrmAppController::get_form_shortcode(array('id' => 20, 'key' => '', 'title' => false, 'description' => false, 'exclude_fields' => '443,478,480,481,482,483,484,485')); ?>
</div>
    
      <?php the_content(); ?>
    </div>
    <footer>
      <?php wp_link_pages(array('before' => '<nav class="page-nav"><p>' . __('Pages:', 'ets'), 'after' => '</p></nav>')); ?>
    </footer>
    <?php comments_template('/templates/comments.php'); ?>
  </article>
<?php endwhile; ?>
