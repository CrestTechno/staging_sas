<?php
/**
 * Custom meta boxes with the CMB plugin
 *
 * @link https://github.com/jaredatch/Custom-Metaboxes-and-Fields-for-WordPress
 * @link https://github.com/jaredatch/Custom-Metaboxes-and-Fields-for-WordPress/wiki/Basic-Usage
 * @link https://github.com/jaredatch/Custom-Metaboxes-and-Fields-for-WordPress/wiki/Field-Types
 * @link https://github.com/jaredatch/Custom-Metaboxes-and-Fields-for-WordPress/wiki/Display-Options
 */

function base_meta_boxes($meta_boxes) {
  /**
   * Page Options meta box
   */
  $meta_boxes[] = array(
    'id'         => 'page_options',
    'title'      => 'Training Course Information',
    'pages'      => array( 'training_courses', ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'fields'     => array(
      array(
				'name' => 'Training Course Information',
				'desc' => 'Information about the Training Course you are adding',
				'id'   => $prefix . 'course_title',
				'type' => 'title',
			),
	   array(
				'name' => 'Course Code',
				'desc' => 'Unique Course ID Code',
				'id'   => $prefix . 'course_code',
				'type' => 'text',
			),	
	   array(
				'name' => 'Course Duration',
				'desc' => 'Duration of the Training Course',
				'id'   => $prefix . 'course_duration',
				'type' => 'text',
			),	
	   array(
				'name' => 'Course Price',
				'desc' => 'Price for the course (A$)',
				'id'   => $prefix . 'course_price',
				'type' => 'text',
			),	
      array(
				'name' => 'Training Detail Section',
				'desc' => 'Details about the training course',
				'id'   => $prefix . 'course_details',
				'type' => 'title',
			),
	   array(
				'name' => 'Course Venue Location',
				'desc' => 'Location of the Training Course',
				'id'   => $prefix . 'course_location',
				'type' => 'text',
			),
	   array(
				'name' => 'Course Start Time',
				'desc' => 'Starting Time of the Course',
				'id'   => $prefix . 'course_starttime',
				'type' => 'text_time',
			),
	   array(
				'name' => 'Course Start Date',
				'desc' => 'Starting Date of the Course',
				'id'   => $prefix . 'course_startdate',
				'type' => 'text_date',
			),
	   array(
				'name' => 'Course End Time',
				'desc' => 'Ending Time of the Course',
				'id'   => $prefix . 'course_endtime',
				'type' => 'text_time',
			),
	   array(
				'name' => 'Course End Date',
				'desc' => 'Ending Date of the Course',
				'id'   => $prefix . 'course_enddate',
				'type' => 'text_date',
			),
	  array(
				'name'    => 'Training Course Delivery Method',
				'desc'    => 'How the course will be delivered',
				'id'      => $prefix . 'course_delivery',
				'type'    => 'multicheck',
				'options' => array(
					'Instructor Led' => 'Instructor Led',
					'Instructor Assisted (mentored)' => 'Instructor Assisted', 
					'Virtual Instructor Led' => 'Virtual Instructor Led', 
					'Webinar' => 'Webinar', 
					'Private One-on-One' => 'Private One-on-One', 
					'Virtual Private One-on-One' => 'Virtual Private One-on-One' 
				),
			),
	   array(
				'name' => 'Course Overview',
				'desc' => 'overview of the Training Course',
				'id'   => $prefix . 'course_overview',
				'type' => 'textarea_small',
			),
	   array(
				'name' => 'Course Pre-Requirements',
				'desc' => 'Requirements of the Training Course',
				'id'   => $prefix . 'course_requirements',
				'type' => 'text',
			),
	   array(
				'name' => 'Course Accreditations',
				'desc' => 'Accreditations of the Training Course',
				'id'   => $prefix . 'course_accreditations',
				'type' => 'text',
			),
		array(
				'name'    => 'Course Information',
				'desc'    => 'Full Information on the Training Course',
				'id'      => $prefix . 'course_information',
				'type'    => 'wysiwyg',
				'options' => array(	'textarea_rows' => 5, ),
			),
      array(
				'name' => 'Course Promotion',
				'desc' => 'Special Offers or Pricing for this Training Course',
				'id'   => $prefix . 'course_promotion',
				'type' => 'title',
			),
	   array(
				'name' => 'Promotion Price',
				'desc' => 'Special Promo Price (A$)',
				'id'   => $prefix . 'course_promoprice',
				'type' => 'text',
			),
	   array(
				'name' => 'Promotion Information',
				'desc' => 'Information about the Promotion',
				'id'   => $prefix . 'course_promoinfo',
				'type' => 'textarea_small',
			),
		array(
				'name' => 'User ID',
				'desc' => 'ID of the User that submitted this course - Do Not Modify',
				'id'   => $prefix . 'vendor_userid',
				'type' => 'text',
			),	
		array(
				'name' => 'Entry ID',
				'desc' => 'Entry ID from the Submission - Do Not Modify',
				'id'   => $prefix . 'vendor_entryid',
				'type' => 'text',
			),
    ),
  );

  return $meta_boxes;
}
add_filter('cmb_meta_boxes', 'base_meta_boxes');

function product_meta_boxes($meta_boxes) {
  /**
   * Page Options meta box
   */
  $meta_boxes[] = array(
    'id'         => 'page_options',
    'title'      => 'Product Information',
    'pages'      => array( 'products', ),
    'context'    => 'normal',
    'priority'   => 'high',
    'show_names' => true,
    'fields'     => array(
      array(
				'name' => 'Product Information',
				'desc' => 'Information about the Product',
				'id'   => $prefix . 'product_title',
				'type' => 'title',
			),
	   array(
				'name' => 'Product Code',
				'desc' => 'Unique Product ID Code',
				'id'   => $prefix . 'product_code',
				'type' => 'text',
			),	
	   array(
				'name' => 'Product Short Description',
				'desc' => 'Short concise description of the product',
				'id'   => $prefix . 'product_shortdesc',
				'type' => 'textarea_small',
			),
		array(
				'name'    => 'Product Full Description',
				'desc'    => 'Full Description of the Product',
				'id'      => $prefix . 'product_fulldesc',
				'type'    => 'wysiwyg',
				'options' => array(	'textarea_rows' => 5, ),
			),
	   array(
				'name' => 'Product Price',
				'desc' => 'Price of the Product - in Australian Dollars - numbers only',
				'id'   => $prefix . 'product_price',
				'type' => 'text',
			),	
	   array(
				'name' => 'Product Discount',
				'desc' => 'Discount for the Product - percentage - number only - optional',
				'id'   => $prefix . 'product_discount',
				'type' => 'text',
			),	
	   array(
				'name' => 'Product Commission',
				'desc' => 'Commission the Product - percentage - number only - ADMIN USE ONLY',
				'std' => '20',
				'id'   => $prefix . 'product_commission',
				'type' => 'text',
			),	
	  array(
				'name'    => 'Physical or Downloadable Product',
				'desc'    => 'How is the product delivered',
				'id'      => $prefix . 'product_delivery',
				'type' => 'radio',
					'options' => array(
					array('name' => 'Physical', 'value' => 'Physical'),
					array('name' => 'Download', 'value' => 'Download')				
					,)
			),
		array(
			'name' => 'Downloadable File',
			'desc' => 'Upload the purchasable file for the customer to download',
			'id' => $prefix . 'product_download',
			'type' => 'file',
			'save_id' => false, // save ID using true
			'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
		),			
		array(
			'name' => 'Sample File',
			'desc' => 'Upload a sample file for the customer to download',
			'id' => $prefix . 'product_sample',
			'type' => 'file',
			'save_id' => false, // save ID using true
			'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
		),
		array(
			'name' => 'Product Image',
			'desc' => 'Upload an image of the product',
			'id' => $prefix . 'product_image',
			'type' => 'file',
			'save_id' => true, // save ID using true
			'allow' => array( 'url', 'attachment' ) // limit to just attachments with array( 'attachment' )
		),
		array(
				'name' => 'Product Gallery',
				'desc' => 'If you have more images to add to your product, attach them via the post gallery',
				'id'   => $prefix . 'product_gallery',
				'type' => 'title',
			),
		array(
				'name' => 'User ID',
				'desc' => 'User ID that submitted the Product - Do Not Modify',
				'id'   => $prefix . 'vendor_userid',
				'type' => 'text',
			),	
		array(
				'name' => 'Entry ID',
				'desc' => 'Entry ID for the Product Submission - Do Not Modify',
				'id'   => $prefix . 'vendor_entryid',
				'type' => 'text',
			),

    ),
  );

  return $meta_boxes;
}
add_filter('cmb_meta_boxes', 'product_meta_boxes');
