��    H      \  a   �            !  	   *     4     A     E  
   R     ]     c     q     }     �  
   �     �     �     �     �     �     �     �     �  
   �       	     2        J     Q     Z     `     e     q     �     �     �     �     �     �     �     �     �     �     �          "  	   .     8  	   F     P     V  	   f     p     v     �     �     �     �     �     �     �  
   �     �  
   �  	   �  $   �  +   	     =	     I	  &   U	  
   |	     �	     �	     �	  �  �	     <     D     T  	   f     p  
   ~     �     �     �     �     �     �     �               #     5     A     [     d     w     �  
   �  :   �     �  	   �     �     �     �                    "     &  #   +     O     h     {  )   �     �     �      �     �               %     5     >     U  	   d     n     ~     �     �     �     �     �     �     �     �     �  
   �  *     $   /     T     b  3   p     �  !   �     �     �     F      3   -   8   &   >      @   ?               +         )   E   9          
   1       #          $      0       G   /       H   B                       ;           2         <   D   '      .   7   *       6   A   !   ,   %             "                   	                  =                   :               (           5   4                C        Activate Add Facet Add Template Any Autocomplete Checkboxes Count Custom Fields Data source Date (Newest) Date (Oldest) Date Range Default Display Code Display Value Dropdown End Date Enter keywords Export Facet Count Facet type Facets Hierarchy How should multiple selections affect the results? Import Imported Label Less License Key License active Max Migrate Min More Narrow the result set Not yet activated Nothing to import Number Range Overwrite existing items? Page Parent term Paste the import code here Post Author Post Date Post Modified Post Type Posts Query Arguments Raw Value Reset Save Changes Search Search engine Settings Settings saved Skipped Slider Sort by Start Date Step Taxonomies Templates The data used to populate this facet The maximum number of facet choices to show Title (A-Z) Title (Z-A) Unable to connect to activation server WP Default Widen the result set expires of Project-Id-Version: FacetWP
POT-Creation-Date: 2014-09-16 19:28-0500
PO-Revision-Date: 2014-09-16 19:28-0500
Last-Translator: FacetWP <hello@uproot.us>
Language-Team: Matt Gibbs <hello@facetwp.com>
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.6.9
X-Poedit-KeywordsList: __;_e
X-Poedit-Basepath: ../
X-Poedit-SearchPath-0: .
 Activar Agregar facetas Agregar plantilla Cualquier Autocompletar Checkboxes Cuenta Campos personalizados Origen de los datos Fecha (más recientes) Fecha (más antiguo) Rango de datos Por omisión Mostrar código Muestra el valor Lista desplegable Fecha final Introducir palabras clave Exportar Número de Facetas Tipo de faceta Faceta Jerarquía ¿Cómo debe afectar el resultado la selección múltiple? Importar Importado Etiqueta Menos Llave de licencia Licencia activa Max Migrar Min Más Restringe el conjunto de resultados No ha sido activado aún Nada para importar Rango numérico ¿Sobreescribir los elementos existentes? Página Término superior Pegar el código importado aquí Autor de entrada Fecha de entrada Entrada modificada Tipo de entrada Entradas Argumentos de consulta Valor primario Restaurar Guardar cambios Buscar Buscador Configuraciones Configuraciones guardadas Saltado Presentación Ordenar por Fecha inicial Paso Taxonomías Plantillas Datos utilizados para publicar esta faceta Máximo número de facetas a mostrar Título (A-Z) Título (Z-A) No es posible conectarse al servidor de activación WP por omisión Amplía el conjunto de resultados expira de 