��    J      l  e   �      P     Q  	   Z     d     q     u  
   �     �     �     �     �     �  
   �     �     �     �     �                    &     5     <  
   H     S  	   Z  2   d     �     �     �     �     �     �     �     �     �     �     �     �     
          )     C     H     T     o  	   {     �  	   �     �     �  	   �     �     �     �     �     �     �     �     	     	  
   	     	  
   $	  	   /	  $   9	  +   ^	     �	     �	  &   �	  
   �	     �	     �	     �	  �  �	     �     �     �     �     �        
             1     D     d     �     �     �     �     �     �     �     �               '     ?     O     X  T   d     �     �     �     �     �     �     �     �            "        /     B     S     h     �     �     �     �     �     �     �     �               '     E  	   a     k          �     �     �     �     �     �  
   �  	   �  2   �  1   '     Y     e  ;   q     �  "   �     �     �     .          ;      :       5   $   #          F   '      -              6                  2       I                 J   A       E   %   D      	              @      8         G   H      =   &      +   >   /          ?      !      1                  4      B   (       "       7                       C      0                 *   
   9           )       ,   3      <        Activate Add Facet Add Template Any Autocomplete Checkboxes Count Custom Fields Data source Date (Newest) Date (Oldest) Date Range Default Delete Facet Delete Template Display Code Display Value Dropdown End Date Enter keywords Export Facet Count Facet type Facets Hierarchy How should multiple selections affect the results? Import Imported Label Less License Key License active Max Migrate Min More Narrow the result set Not yet activated Nothing to import Number Range Overwrite existing items? Page Parent term Paste the import code here Post Author Post Date Post Modified Post Type Posts Query Arguments Raw Value Reset Save Changes Search Search engine Settings Settings saved Skipped Slider Sort by Start Date Step Taxonomies Templates The data used to populate this facet The maximum number of facet choices to show Title (A-Z) Title (Z-A) Unable to connect to activation server WP Default Widen the result set expires of Project-Id-Version: FacetWP
POT-Creation-Date: 2014-09-16 19:28-0500
PO-Revision-Date: 2014-09-16 19:28-0500
Last-Translator: FacetWP <hello@uproot.us>
Language-Team: Matt Gibbs <hello@facetwp.com>
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.6.9
X-Poedit-KeywordsList: __;_e
X-Poedit-Basepath: ../
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SearchPath-0: .
 Activer Ajouter une facette Ajouter template N'importe lequel Autocompléter Cases à cocher Dénombrer Champs personnalisés Source de données Date (plus récente en premier) Date (plus ancienne en premier) Éventail de dates Par défaut Effacer la facette Effacer template Montrer le code Valeur à afficher Bande déroulante Date de fin Entrer les mots-clés Exporter Dénombrer les facettes Type de facette Facettes Hiérarchie De quelle manière des sélections multiples doivent-elles affecter les résultats ? Importer Importé Titre Moins Clé d'activation License active Max Migrer Min Plus Réduire l'ensemble des résultats Pas encore activé Rien à importer Éventail de nombres Écraser les items existants ? Page Terme parent Coller le code importé ici Auteur de l'article Date d'article Article modifié Type d'article Articles Arguments de la requête Valeur brute Remettre à l'état d'origine Enregistrer les changements Recherche Moteur de recherche Paramètres Configuration sauvegardée Étape sautée Curseur Classer par Date de début Étape Taxonomies Templates Les données utilisées pour remplir cette facette Le nombre maximum de choix de facettes à montrer Titre (A-Z) Titre (Z-A) Impossible de se connecter sur le serveur pour l'activation WP par WP par défaut Élargir l'ensemble des résultats expire de 