��    ]           �      �     �     �  	                       +     4  
   9     D     J     X     d     r  
   �     �     �     �     �     �     �     �     �     �     �     	  
   	     	     	     .	     5	  	   B	  2   L	     	     �	      �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     
     
     (
     5
     O
     T
     `
     {
  	   �
     �
  
   �
  	   �
     �
     �
     �
  	   �
     �
     �
     �
     �
                    #  ,   2     _     k     s     z  
   �     �  
   �  	   �  *   �  (   �  $   �  +         L     ^     j  &   v  
   �     �     �     �     �  G  �  
             1     E     ^     c  	   �     �     �     �     �     �     �     �                    2     C  '   U     }     �     �     �     �     �     �     �     �     �        
     ;        T  
   `  0   k     �     �     �     �     �  	   �     �     �     �          	  !        @  "   N     q  !   w  #   �     �     �     �     �                         /     4     C     S     a     w     }     �  %   �  B   �                     $     3     @  	   H  	   R      \  :   }  5   �  )   �          %     3  E   A     �     �     �     �     �     ;   Q          6       Z              "              ?   8   '   3   %   L   -   V              )          
   5   !   O   +          U   J      ,       X   @              0      *      I       [   :      1   G      4   #   =   B   N   .   \                 C   9       /       Y       D          E   R   P   S   H              F         A       7       (               >       W      2   	   <             ]               K   $         T              M   &                    Activate Activate license Add Facet Add Template Any Autocomplete Behavior Both Checkboxes Count Custom Fields Data source Date (Newest) Date (Oldest) Date Range Default Default label Delete Facet Delete Template Display Code Display Value Dropdown End Date Enter keywords Export Facet Count Facet type Facets Fields to show Format Hierarchical Hierarchy How should multiple selections affect the results? Import Imported Is this a hierarchical taxonomy? Label Less License Key License active Max Migrate Min More Narrow the result set No Not yet activated Nothing to import Number Range Overwrite existing items? Page Parent term Paste the import code here Post Author Post Date Post Modified Post Title Post Type Posts Prefix Query Arguments Raw Value Re-index Renew license Reset Save Changes Search Search engine Settings Settings saved Show choices that would return zero results? Show ghosts Skipped Slider Sort by Start Date Step Taxonomies Templates Text that appears before each slider value The amount of increase between intervals The data used to populate this facet The maximum number of facet choices to show The number format Title (A-Z) Title (Z-A) Unable to connect to activation server WP Default Widen the result set Yes expires of Project-Id-Version: FacetWP
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-09-29 09:46-0500
PO-Revision-Date: 2014-09-29 09:46-0500
Last-Translator: FacetWP <hello@uproot.us>
Language-Team: Marie Hoppe <info@mk-hoppe.de>
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Poedit-SourceCharset: UTF-8
X-Loco-Source-Locale: de_DE
X-Generator: Poedit 1.6.9
X-Poedit-KeywordsList: __;_e
X-Poedit-Basepath: ../
X-Loco-Parser: loco_parse_po
X-Loco-Target-Locale: en_GB
X-Poedit-SearchPath-0: .
 aktivieren Lizenz aktivieren Facette hinzufügen Neues Template erstellen Alle Automatisch vervollständigen Verhalten Beides Kontrollkästchen (Checkbox) Anzahl Benutzerdefinierte Felder Datenquelle Datum (Neueste zuerst) Datum (Älteste zuerst) Zeitraum Standard Standardbeschriftung Facette löschen Template löschen Code zur Darstellung der Suchergebnisse Anzeigewert Dropdown Enddatum Stichwörter eingeben Exportieren Anzahl Ergebnisse Facetten-Typ Facetten Anzuzeigende Felder Format Hierarchisch Hierarchie Wie soll sich Mehrfachauswahl auf die Ergebnisse auswirken? Importieren Importiert Handelt es sich um eine hierarchische Taxonomie? Beschriftung weniger Lizenzschlüssel Lizenz ist aktiv max Migration min mehr Suchergebnisse verfeinern Nein Noch nicht aktiviert Keine Daten für Import gefunden. Zahlenbereich Bestehende Objekte überschreiben? Seite Übergeordneter Taxonomie-Begriff Fügen Sie den Import-Code hier ein Beitragsautor Veröffentlichungsdatum Letzte Aktualisierung Beitragstitel Post Typ Posts Präfix Abfrageparameter Wert Neu indexieren Lizenz erneuern Zurücksetzen Änderungen speichern Suche Such-Engine Einstellungen Die Einstellungen wurden gespeichert. Auswahlmöglichkeiten anzeigen, die keine Treffer ergeben würden? Leere anzeigen Übersprungen Slider Sortieren nach Anfangsdatum Schritt Taxonomie Templates Präfix für jedes Slider-Object Intervallgröße (Menge, um die je Intervall erhöht wird) Welche Daten sind für diese Facette ausschlaggebend? Maximale Anzahl von Auswahlmöglichkeiten Zahlenformat Titel (A - Z) Titel (Z - A) Es konnte keine Verbindung zum Aktivierungsserver hergestellt werden. WP Standard Suchergebnisse erweitern Ja Läuft ab am von 