<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/** Enable W3 Total Cache Edge Mode */
define('W3TC_EDGE_MODE', true); // Added by W3 Total Cache

ini_set('max_execution_time', 600); //300 seconds = 5 minutes

define('WP_MEMORY_LIMIT', '600M');

/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */
 define('DISABLE_CACHE', true);
define('WP_SITEURL','http://127.0.0.1/staging_sas');

define('WP_HOME','http://127.0.0.1/staging_sas');

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 's-a-s');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'dy.Fqi`uC-(7Ms=HFncG]lp<?K-zIkQ+fKzQ{$N#]2y__YtLTaSqd|s]!!(>=HZ,');
define('SECURE_AUTH_KEY',  '&dQ((?y{!}.n+])-#,?{I@nA4*w5$:f:J1mh~QqRv+67t^d#^wX;yfLY|g0@im4J');
define('LOGGED_IN_KEY',    '5pLni++VP-6Ue(S5LD3:*-?(ueDw~^+$ D9iak(%ISy#^V|XBz.?L~P.b+>DLQ&e');
define('NONCE_KEY',        '5db(%]x@25trev!XTUIeHg{ !o}N+-Y7a>f[q_qo{UF0Dk+d!> M+-gGS)n:1Hrm');
define('AUTH_SALT',        'GWh(z?}}GJ9NMa:<rg7IoN3Sc9)%UeP^>QO)TK)4)*GSzXZ6.rlMGvxC]`f-HDG0');
define('SECURE_AUTH_SALT', '#L1Wa3E.X|8b:0%OVn1d.Ad%#9>=,9oXj)3 .eD+AiY1C$F-57j/$ATc#PV|Tfov');
define('LOGGED_IN_SALT',   'A5ii3&?fBU]h} jN!qaDv+an[oQ_iJCCpkWY&K9]+S#Kn{m-]9hbs!NL{9:[a%Q|');
define('NONCE_SALT',       'dbX_EG?6BDwcu;iUyE]%WE&~Egel<a*[vZW7N?wu{pZqE_31#@}{US?rp-6j<nt2');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
